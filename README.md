This is a Rust implementation of some of the functionnality of the Maestro
sequencer used at Environment Canada on the super computers.

In the original Maestro, the path of a node is passed as an argument and the
code looks through the XML files for info on that particular node.

In this implementation however, the XMLs are parsed into a tree of Node data
structures representing the whole stuite.

```rust
type NodeRef Rc<RefCell<Node>>
type WeakNodeRef Weak<RefCell<Node>>
struct Node {
    name: String,
    flow_type: FlowType,
    depends_on: Vec<DepData>;
    children: Vec<NodeRef>,
    submits: Vec<WeakNodeRef>,
    parent: WeakNodeRef,
    submitter: WeakNodeRef,
}
```

There are two trees defined by the nodes, one is the tree formed by the
submits-submitter relation which is the one that we see in XFlow, the other tree is
formed by the children-parent relation.

The `cargo run` command will compile and run the program, which will create a
server on `localhost:8080`.

The URL `localhost:8080/list/absolute/path` will show the directory contents of
`/absolute/path` for navigation.  When the directory of an experiment is clicked
the experiment graph will be displayed.

In the experiment graph, nodes can be clicked to display a not-yet-implemented
nodeinfo popup.

<!-- README.md uses `GFM` format "GitLab Flavored Markdown" see https://docs.gitlab.com/ee/user/markdown.html-->
<!-- Please go to the gitlab project to see the rendered text.-->

![Maestro module cleanup](http://hpfx.science.gc.ca/~jmo000/CMDI/cleanup_image.png)

`cleanup` module
==============
The `cleanup` module makes a cleanup of all the working files not needed anymore online for the suite. 

Task Descriptions
-----------------

* seqcleanup: Erases all files in the `${SEQ_EXP_HOME}/sequencing`, all the links in `${SEQ_EXP_HOME}/listings/latest` for the date with a `${CLEANUP_seq_delay}` delay with the current date and `${SEQ_EXP_HOME}/logs` for the date with a `${CLEANUP_logs_delay}` delay with the current date.
* frontend_cleanup
  * erases the working directory for the current date (or with a delay specified by the variable `${CLEANUP_work_delay`}
  * erases the listings for the date with a delay of `${CLEANUP_listings_delay}`
  * erases all the other 'hubtype' in the list given by `${CLEANUP_hubtypes}` (banco or gridpt for example) for valid at a date with a delay of `${CLEANUP_hubtype_delay}`
* backend_cleanup
  * erases the working directory for the current date (or with a delay specified by the variable `${CLEANUP_work_delay}`
  * erases the listings for the date with a delay of `${CLEANUP_listings_delay}`
  * erases all the other 'hubtype' in the list given by `${CLEANUP_hubtypes}` (banco or gridpt for example) for valid at a date with a delay of `${CLEANUP_hubtype_delay}` 


Module Interface
----------------
| Interface Variable            | Default Value                                       | Description |
| :-----------------            | :-------------------------------------------------- | :---------- |
| CLEANUP_CFG   	            | `${SEQ_EXP_HOME}/config/${SEQ_CURRENT_CONTAINER}.cfg` | Path to a configuration file which defines some variables of the interface using the tool [source_cfg.dot](https://wiki.cmc.ec.gc.ca/wiki/Assimilation/Maestro/Documentation#source_cfg.dot) through variable `${DOT_CFG}` which must be defined before (in experiment.cfg for example). |
| CLEANUP_DATE                  | `${SEQ_SHORT_DATE}` Maestro environment variable      | This variable tells the observations date. It is very useful when building Unit Tests
| CLEANUP_utils_ssm | eccc/mrd/rpn/utils/16.2.2 | RPN utils ssm domain
| CLEANUP_ovbin | `${SEQ_EXP_HOME}/config/${SEQ_CURRENT_CONTAINER}/ovbin` |Path to use when we want to overload a binary used by the coldstart module 
| CLEANUP_concurrent | 1 |
| CLEANUP_listings_delay | 6 | Delay from the current date for which the listings will be erased
| CLEANUP_listings_type  | success | Types of listing to be erased: can be 'success', 'abort' or both with 'success abort' 
| CLEANUP_hubtype_delay | 6 | Delay from the current date for which the files in the directories like `${SEQ_EXP_HOME}/hub/ade/${TRUE_HOST}` or `${SEQ_EXP_HOME}/hub/gridpt/${TRUE_HOST}` will be erased
| CLEANUP_hubtypes      | 'gridpt banco verif' | list of hubtypes  (this may be empty) 
| CLEANUP_seq_delay | 6 | Delay from the current date for which the sequencing files in the directory `${SEQ_EXP_HOME}/sequencing` and and links in `${SEQ_EXP_HOME}/listings/latest` will be erased
| CLEANUP_seq_status_delay | infinite | This variable controls the cleanup of the directory `${SEQ_EXP_HOME}/sequencing/status`
| CLEANUP_logs_delay | infinite | This variable controls the cleanup of the directory `${SEQ_EXP_HOME}/logs`
| CLEANUP_work_delay | 0 | Delay from the current date for which the working directory `${SEQ_EXP_HOME}/hub/work/${TRUE_HOST}/${DATE}` will be erased
| CLEANUP_filter | '<no value>' | This variable indicates a filter with which you can decide the retention delay for each file. The [syntax of filter] (README.md#syntax-of-the-filter) is explained below.
| CLEANUP_tool | cleanup.py | This can be cleanup.ksh or cleanup.py. We strongly recommand the second one since it is a lot faster.
| CLEANUP_Do_Delete | yes | If yes, then it really erases the files. If not, then it won't erase the files. This is useful for debugging the modules or testing filters. 
| CLEANUP_VERBOSE_OPTION | no |


Configuration in your experiment
--------------------------------
It is recommended to keep the configuration of the archive module in its default setting (`${CLEANUP_CFG}`) with a directory cleanup containing files for the configuration.

example:
`${SEQ_EXP_HOME}/config/main/cleanup.cfg` 

The variable `CLEANUP_Do_Delete` must be set to "yes", to activate the cleaning.  The `cleanup` module uses 'filter' files to select which files should be deleted and when. The `hub` subdirectories to be cleaned are specified with the module variable `CLEANUP_hubtypes`.

### Syntax of the filter
A filter may be used to detail the files to be cleaned, specified by the [interface variables] (README.md#module-interface). A filter is made of a file with several lines containing each two elements:
* shell glob patterns and may include some shell variables available at the time the filter is interpreted and
* a number which specifies the delay, in hours, after which the file will be erased 

For example, the filter
```
  dynbcor 240
```
will match the file
```
  ${SEQ_HOME_EXP}/hub/hadar/dynbcor/coeffs/amsua/2011012000_coeffs_amsua
```
if the `${SEQ_DATE}` is greater than 20110130000000 (10 days after, the file date).

This filter only controls the cleanup of the elements under `${SEQ_HOME_EXP}/hub` other than `${SEQ_HOME_EXP}/hub/${TRUE_HOST}/work` and `${SEQ_HOME_EXP}/hub/${TRUE_HOST}/listings`. 

Access to the code
------------------
The official repository of the code is :
```bash
https://gitlab.science.gc.ca/CMDI/maestro_module_cleanup
```

This module is meant to be included in a maestro experiment as a [`git-subtree`](https://wiki.cmc.ec.gc.ca/wiki/Git_subtree).


#!/bin/ksh
#.*****************************************************************************
#.
#.     JOB NAME - seqcleanup 
#.
#.     STATUS - NON-ESSENTIAL
#.
#.     DESCRIPTION - erases old files in the directory ${SEQ_EXP_HOME}/sequencing and ${SEQ_EXP_HOME}/logs
#.
#.*****************************************************************************

#--------------------------------------------------------------------------------------------------
# We are not using the ${TASK_WORK} directory because it will be erased by some of the other tasks in that module
#--------------------------------------------------------------------------------------------------
cd ${TMPDIR}

if [ "${CLEANUP_seq_delay}" != 'infinite' ]; then
    previous_date=$(r.date ${SEQ_DATE} -${CLEANUP_seq_delay} | cut -c-14)

    seq_dir=$(${TASK_BIN}/true_path ${TASK_INPUT}/seq_dir)
    if [ -n "${seq_dir}" ]; then
	$TASK_BIN/nodelogger -n $SEQ_NODE -s infox -m  "Cleanup Sequencing Directory ${seq_dir}"
	cd ${seq_dir}/output
        find . -type f -path "*${SEQ_CONTAINER%/*}*${previous_date}*" | grep -v "${SEQ_NODE}\.${SEQ_DATE}\.pgmout[0-9]*" | xargs ${DELETE_COMMAND}
	if [ "${CLEANUP_seq_status_delay}" != infinite ]; then
	    cd ${seq_dir}/status
            ## Si on fait le cleanup pour la date courante, il faut faire attention de ne pas effacer
	    ## les .end de tous les modules apparente a ${SEQ_CONTAINER}.
	    status_previous_date=$(r.date ${SEQ_DATE} -${CLEANUP_seq_status_delay} | cut -c-14)
	    if [[ "${status_previous_date}" = ${SEQ_DATE} ]]; then
		find . -type f -path "*${status_previous_date}${SEQ_CONTAINER%/*}/*" | grep -v "${status_previous_date}${SEQ_CONTAINER%/*}/[^/]*\.end" | grep -v "${status_previous_date}${SEQ_CONTAINER}/[^/]*\.end" | xargs ${DELETE_COMMAND}
	    else
		find . -type f -path "*${status_previous_date}${SEQ_CONTAINER%/*}/*" | xargs ${DELETE_COMMAND}
	    fi
	fi
	cd ${seq_dir}/wrapped
        find . -type f -path "*${SEQ_CONTAINER%/*}*${previous_date}*" | xargs ${DELETE_COMMAND}
	cd ${seq_dir}/batch
        find . -type f -path "*${SEQ_CONTAINER%/*}*${previous_date}*" | xargs ${DELETE_COMMAND}
    fi

    list_dir=$(${TASK_BIN}/true_path ${TASK_INPUT}/listings)
    if [ -n "${list_dir}" ]; then
	$TASK_BIN/nodelogger -n $SEQ_NODE -s infox -m  "Cleanup listings ${list_dir}"
	cd ${list_dir}
        find . -type l -path "*${SEQ_CONTAINER%/*}*${previous_date}*" | xargs ${DELETE_COMMAND}
    fi
else
    $TASK_BIN/nodelogger -n $SEQ_NODE -s infox -m  "Nothing to be erased in Sequencing Directory since 'CLEANUP_seq_delay=${CLEANUP_seq_delay}'"
    return
fi

if [ "${CLEANUP_logs_delay}" != 'infinite' ]; then
    previous_date=$(r.date ${SEQ_DATE} -${CLEANUP_logs_delay} | cut -c-14)

    logs_dir=$(${TASK_BIN}/true_path ${TASK_INPUT}/logs)
    if [ -n "${logs_dir}" ]; then
	$TASK_BIN/nodelogger -n $SEQ_NODE -s infox -m  "Cleanup Logs Directory ${logs_dir}"
	cd ${logs_dir}
        find . -type f -name "${previous_date}_[nt]*" | xargs ${DELETE_COMMAND}
    fi
else
    $TASK_BIN/nodelogger -n $SEQ_NODE -s infox -m  "Nothing to be erased in Logs Directory since 'CLEANUP_logs_delay=${CLEANUP_logs_delay}'"
    return
fi

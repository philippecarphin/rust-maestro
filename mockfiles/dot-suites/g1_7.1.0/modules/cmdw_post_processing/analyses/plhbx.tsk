#!/bin/ksh
#***
#.*****************************************************************************
#.
#.     JOB NAME -  g1plhbx_y2k
#.
#.     STATUS - OPERATIONAL - semi-essential
#.
#.     ORIGINATOR -  Gilles Desautels
#.
#.     PRODUCT - plot chart
#.       
#.               plot_#        printed   time avbl(GMT)   description 
#.               ------        -------   -------------    -----------
#.
#.     cmc_plt   plhbx00         no          n/a          925mb anal 00z
#.     cmc_plt   plhbx06         no          n/a          925mb anal 06z
#.     cmc_plt   plhbx12         no          n/a          925mb anal 12z
#.     cmc_plt   plhbx18         no          n/a          925mb anal 18z
#.
#.
#.     DESCRIPTION - standard 925mb anal (not XMIT copy).
#.
#.     REVISION 1.1:   Conversion AN 2000
#                      Gilles Desautels - 21 aout 1998
#
#                      NOTE: on fait simplement un pointage de carte
#                            les fonctions RPN sont les seules ici utilisees
#                            aucun changement requis
#                            (ici modification pour les test AN 2000)
#.
#.     REVISION 2.0:   Marc Klasa - 17 avril 2000
#.                     Ce script est base sur g1plhax et a ete modifie pour avoir
#.                     un nouveau modele de pointage qui pointe les donnees non-vues
#.                     en gras (sans le cercle pointille)
#.
#.*****************************************************************************

#.*****************************************************************************
#.    REVISION 1.2    Conversion a Maestro
#.                    Naysan Saran - 11 Dec. 2013
#.*****************************************************************************  


#
# btyp est 70 pour les fichiers vus par l'OA
#          66 pour les fichiers des donnees derivees, entree a l'AO
# btyp est 71 pour global
#          67 pour global
#
btyp='70'
btyp2='66'
MXVA=9
MYVA=9

#
# nivplt est le niveau du pointage en daPa (decaPascal)
# niv est aussi utilise comme ${niv} pour specifier le niveau dans
#     les appels VARIAN, HAFTON,
#===========
niv='925'
nivplt='9250'
#===========
#

if [ ${RUNHOUR} =  "00" ] ; then
   temps='00,23,01,22,02,21'
fi
if [ ${RUNHOUR} =  "12" ] ; then
   temps='12,11,13,10,14,09'
fi

#
# nom du fichier burp a utiliser
#
nom_fichier_burp=${TASK_INPUT}/fichier_burp
ln -s $nom_fichier_burp .
nom_fichier_burp=`basename $nom_fichier_burp`
echo nom_fichier_burp=$nom_fichier_burp

#
# et le fichier standard pour pgsm (il pourrait y en avoir plus d'un
#
nom_fichier_pgsm1=${TASK_INPUT}/fichier_pgsm
echo $nom_fichier_pgsm1

###############################################################################
#  pgsm va chercher les champs dans les projections desirees
#  les cartes de controles pour pgsm
#
rm -f pgsmdir
cat << EOFPGSM > pgsmdir
#
* DIRECTIVES PGSM QUI SERVENT A R1PLT01
* (SERIE GRANDE CARTE ANALYSE)
*******************************************************************************
* 09 juil 1992
* GILLES DESAUTELS (DDO)
*------------------------------------------------------------------------------
*                                                                          PGSM
*------------------------------------------------------------------------------
 SORTIE(STD,15,A)
 EXTRAP(VOISIN)
 SETINTX (LINEAIR)
*
 GRILLE(PS,286,211, 151.0, 151.0,  50800., 21.0, NORD)
 COMPAC= -16
 HEURE(0) 
*                                  CHAMPS GRANDES CARTES
 CHAMP(Z,${niv}) 
 CHAMP(T,${niv}) 
*
*------------------------------------------------------------------------------
*                                                                     FIN  PGSM
*------------------------------------------------------------------------------


EOFPGSM
#
#  execution du pgsm
#
rm -f femap
${TASK_BIN}/pgsm -iment $nom_fichier_pgsm1 \
          -ozsrt femap \
          -i pgsmdir
rm -f pgsmdir

###############################################################################
rm -f sigmadir
cat << EOFSIGMA > sigmadir
#
*******************************************************************************
* 09 JUIL 1992
* GILLES DESAUTELS (DDO)
* ajout pointage 8-jan-92
*******************************************************************************
*                DIRECTIVES SIGMA
*                DIRECTIVES GLOBALES
*******************************************************************************
 CLIP = OUI
 NORMID = NON
 LISSAGE=OUI
*
*LA FONTE 15 CONTIENT LES MAJUSCULES, LA 16 LES MINUSCULES
*LA FONTE 10 CONTIENT LES LETTRES MAJUSCULES DES H,L
*LA FONTE 11 CONTIENT LES LETTRES MINUSCULES CORRESPONDANTES
*LA FONTE 17 CONTIENT LES CHIFFRES DES ETIQUETTES (FONTE ETROITE)
*LA FONTE  2 CONTIENT LES GRANDS + ET -
*LA FONTE  3 CONTIENT LES GRANDS LETTRES GRECQUES
*LA FONTE  9 CONTIENT LE CENTRE (X DANS O)
*
*   @         A  B  C  D  E  F  G  H  I
*   "CASE" 0  1  2  3  4  5  6  7  8  9
 SETFONT([15,16,10,11,12,13, 2, 3,17, 9])
*
* OPTIONS GEOGRAPHIQUES
*
 MAPLAB="ST",1,1,1,1,10,2,NON,OUI,NON,1,.5,OUI
 MAPCOLR=1
*
* EXPLICATION CONTINENTS-MERIDIEN-PARALLELES
* ET PERIMETRE DE LA CARTE (="YES" PAR DEFAUT)
*
 OPTIONS(MAPOPT,["OU","GL","GR",-1.0,"LA","NO"]) 
 OPTIONS(DASHSET,["SMALL",16.0,"NP",1500])
*
* EN UTILISANT 30.0 POUCES (LA CARTE FAIT 20 X 28.5 )
*    LES DISTANCES RELATIVES SONT VOISINES DU 1/100 PO
* ON DOIT UTILISER SIZE=3000 DANS TRAMES
*
 ECHELLE=20.0 
 POUCES=30.0
*
 ECHLGRI=NON
*
* PRECISE UN POINTILLE FIN (TOUJOURS INCLURE "NCRT",1)
*
 OPTIONS(ISPSET,["NCRT",1])
*
* 
* SETHPAL NECESSAIRE POUR PRODUIRE EFFACEMENT AVEC MOTIF 1
*                         COLORIER LE MOTIF 2 (=099) (VIOLET - TRANSPARENT)
*                         COLORIER LE MOTIF 3 (=109) (VIOLET - TRANSPARENT)
*
 SETHPAL([0001001,0516099,0516109],3)
*
************************************************************************........
*                    GROUPE
************************************************************************........
* GROUPE LOT=1-2   POUR LES ISOHYPSES DES HAUTEUR     1=majeur
*                  COULEUR CO=7 (BLEU)                2=mineur
*
 GROUPE(1,["CO",7,"TH",8,
   "CA",8,"FC",0,"BC",7,"CF","YES","IN",2,"SI",200,"CN",34])
 GROUPE(2,["CO",7,"TH",4,
   "CA",8,"FC",0,"BC",7,"CF","YES","IN",2,"SI",200,"CN",34])
*
* GROUPE LOT=3-4-5 POUR LA TAILLE DES MAX-MIN DES HAUTEURS
*
 GROUPE(3,["SI",900,"IN",8,"FC",7,"ENH","YES","CN",01]) 
 GROUPE(4,["SI",175,"IN",2,"FC",7,"ENH","YES","CN",01]) 
 GROUPE(5,["SI",225,"IN",3,"FC",7,"ENH","YES","CN",01,"CA",8]) 
*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
* GROUPE LOT=6-7   POUR LES ISOHYPSES DES TEMPERATURES
*                  COULEUR CO=5 (VIOLET)
*
*        majeur=7, mineur=6
*
 GROUPE(6,["CO",5,"TH",3,
           "CA",8,"FC",5,"BOX","YES","CLR","YES",
                  "IN",2,"SI",200,"CN",34]) 
 GROUPE(7,["CO",5,"TH",6,
           "CA",8,"FC",5,"BOX","YES","CLR","YES",
                  "IN",2,"SI",200,"CN",34]) 
*
* GROUPE  9 POUR DEFINIR TAILLE  MIN-MAX UTILISE PAR + -
* GROUPE 10 POUR DEFINIR VALEUR MIN-MAX DES TEMPERATURES (AVEC ENH=-1)
*
 GROUPE( 9,["FC",5,"SI",225,"IN",3,"ENH",-1,"CN",01]) 
 GROUPE(10,["FC",5,"SI",200,"IN",2,"ENH",-1,"CN",01,"CA",8])
*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
* GROUPE LOT=11    POUR LES PATRONS (-2.5 < T < +2.5)
*                  COULEUR CO=5 (VIOLET)
*
 GROUPE(11,["CO",5,"TH",1])
*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
* ECRITURE ID CARTE
*
* GROUPE 25 POUR TITRE ANALYSIS-ANALYSE
* GROUPE 26 POUR ID DU CMC ET DESCRIPTION DU CONTENU
*        27      HEURE DE VALIDITE
*        28      CONTENU DESCRIPTIF
*        29      TITRE DE LA CARTE (quand le titre est trop long)
* "CA",0 => FONTE 15  (defaut)
* "CA",8 => FONTE  8  (etroit)
*
 GROUPE(25,["CA",0,"SI",225,"IN",3,"FC",1,"CN",01])
 GROUPE(26,["CA",0,"SI",150,"IN",1,"FC",1,"CN",01])
 GROUPE(27,["CA",0,"SI",110,"IN",2,"FC",1,"CN",01])
 GROUPE(28,["CA",0,"SI",100,"IN",1,"FC",1,"CN",01])
 GROUPE(29,["CA",0,"SI",175,"IN",4,"FC",1,"CN",01])
************************************************************************........
*LE NUMERO DE LA CARTE OPERATIONNELLE
*
 MUMS([" HBX"," HBX"," HBX"," HBX"])
************************************************************************........
 PANNEAU([0.0, 0.0])
************************************************************************........
*             CARTE ${niv}=925 MB
************************************************************************........
*DIRECTIVES POUR LES ETIQUETTES DES TEMPERATURES
* 
 OPTIONS(ISPSET,["RNDL",1., "RNDV",1.]) 
 OPTIONS(ISPSET,["ECOL",250000850, "LABL",6, "ILAB",1])
 OPTIONS(ISPSET,["MAJR",7, "MINR",6, "NULB",13])
*  
*DIRECTIVES POUR LES MAX-MIN DES TEMPERATURES
* 
 OPTIONS(ISPSET,["HCAR","@F+","LCAR","@F-","CCAR","?","LHCE",2,"NHL",3])
 OPTIONS(ISPSET,["HILO",09,"CNTR",09,"VALU",10]) 
* 
*DIRECTIVES POUR LES LIGNES DE CONTOUR DES TEMPERATURES
*PRECISE POUR CHAQUE CHAMP LE FACTEUR MULTIPLICATIF ET INTERVALLE CONTOUR
*(ENONCE OBLIGATOIRE)  RECOMMANDE DE NE PAS MODIFIER SCAL EN COURS DE ROUTE
*
 SCAL("TT",1.0 , 5.) 
*
 NIVEAUX = -2.5, +2.5
* MOTIFS =000,002,000
* HAFTON("TT", -1,-1, ${niv},0,-1,[GEO,NIV,PAT])
*
* la superposition d un 2e motif est plus jolie
*
 MOTIFS =000,003,000
 HAFTON("TT", -1,-1, ${niv},0,-1,[GEO,NIV,PAT])
*
 LIMITE=-70.,+70. 
 DASH([1430B])
 OPTIONS(ISPSET,["NCRT",1])
*
* OFFM FAIT QUE LE MESSAGE EN BAS DU GRAPHIQUE EST ELIMINE
* MXVA ET MYVA PERMET D'ELIMINER LES MAX-MIN TROP RAPPROCHES
* ON PEUT FAIRE VARIER CES NOMBRES SELON LE BRUIT DU CHAMP
*
 OPTIONS(ISPSET,["OFFM",1, "MXVA",$MXVA, "MYVA",$MYVA])
* 
*
 VARIAN("TT", -1,-1, ${niv},0,-1,[GEO])
*-----------------------------------------------------------------------------
*DIRECTIVES POUR LES LIGNES DE CONTOUR DES HAUTEURS
*PRECISE POUR CHAQUE CHAMP LE FACTEUR MULTIPLICATIF ET INTERVALLE CONTOUR
*(ENONCE OBLIGATOIRE)  RECOMMANDE DE NE PAS MODIFIER SCAL EN COURS DE ROUTE
*
 SCAL ("GZ",   1.0, 3.0)
* 
*DIRECTIVES POUR LES ETIQUETTES DES HAUTEURS
* 
 OPTIONS(ISPSET,["RNDL",1., "RNDV",1.]) 
 OPTIONS(ISPSET,["ECOL",350000950, "LABL",1, "ILAB",1])
 OPTIONS(ISPSET,["MAJR",1, "MINR",2, "NULB",7])
* 
*DIRECTIVES POUR LES MAX-MIN DES HAUTEURS
* 
 OPTIONS(ISPSET,["HCAR","@BH","LCAR","@BL","CCAR","@I9","LHCE",1,"NHL",3])
 OPTIONS(ISPSET,["HILO",3,"CNTR",4,"VALU",5]) 
* 
*DIRECTIVES POUR LES LIGNES DE CONTOUR DES HAUTEURS
*
 LIMITE=-18., 156.
 DASH([1777B])
 OPTIONS(ISPSET,["NCRT",1])
*
* OFFM FAIT QUE LE MESSAGE EN BAS DU GRAPHIQUE EST ELIMINE
*
* MXVA ET MYVA PERMET D'ELIMINER LES MAX-MIN TROP RAPPROCHES
* ON PEUT FAIRE VARIER CES NOMBRES SELON LE BRUIT DU CHAMP
*
 OPTIONS(ISPSET,["OFFM",1, "MXVA",$MXVA, "MYVA",$MYVA])
*
*
 VARIAN("GZ", -1,-1, ${niv},0,-1,[GEO])
**********************************************************************
**********************************************************************
**********************************************************************
**********************************************************************
**********************************************************************
**********************************************************************
*  DIRECTIVE POUR LE POINTAGE
*  choix des fonctions de selection
**********************************************************************
* on doit mettre les fontes car le pointage a besoin des fontes
* de defaut (position 0) ainsi que 6-7-8-9 (symboles meteo)
*
 SETFONT([15,01,02,03,04,05,06,07,08,09])
*---------------------------------------------------------------------
* utilise la distance CDIS pour eliminer les stations trop rapprochees
*
* utilise la distance FDIS (Frame DIStance) pour ne pas avoir du pointage
*         qui va en dehors de la carte
*
 PLTSEL( 'CDIS', 350 )
 PLTSEL( 'FDIS', 600 )
 PLTSEL( 'SIZE', 100 )
**********************************************************************
*facteur multiplicatif des intensites par rapport a un modele V200
*en unite entiere de 1/100   (defaut 100)
*
 PLTSEL( 'FINT', 100)
**********************************************************************
* PLTSEL( 'FLGS', -1 )   c est le defaut
*---------------------------------------------------------------------
* pour mettre un  ordre de priorite dans les stations
*
* PLTSEL( 'STNS', ['station1','station2','station3',...])
*                   station 1 a N+1  prio ritaire sur les autres
* PLTSEL( 'STNS', ['stationN','stationN+1',])
* PLTSEL( 'STNS', '*********' )                            fait les autres apres
 PLTSEL( 'STNS', ['71603','*****'])
*---------------------------------------------------------------------
* ordre de selection des heures
* on utilise la variable locale definie dans le script
* EX.:* PLTSEL ('TIME', [12,11,13,10,14,09])
 PLTSEL( 'TIME', [$temps] )
*---------------------------------------------------------------------
* fait l'orientation au pole des stations si =1
* si 0 alors on ecrit par rapport au bas du papier
*
 PLTSEL( 'ORTP', 0 )
*---------------------------------------------------------------------
# ne pas considerer les stations dont le <flag> est 32 ou 256
# ce qui signifie hors du domaine pour 32
#                 non utilise par l AO pour 256
 PLTSEL( 'RFLG', [32,256] )

***MVE  Ceci corresponds au niveau de pression qu'on veut matcher contre 9250 plutot que 925
* sinon on matche les 925,x au lieu de 925,0
 PLTCVT(7004,0.0,0.1)
*---------------------------------------------------------------------
* RELM permet de ne pas imprimer le code BUFR 7004 s'il est a 700
* RELM permet de ne pas imprimer le code BUFR 4198 s'il est a  12
* pour annuler il faut appeler ainsi: PLTSEL('RELM",-1)
*
* il est possible de n'avoir qu'un couple dans les [  ]
*
 PLTSEL('RELM',[7004,$nivplt,4198, $RUNHOUR])
*

*---------------------------------------------------------------------
*                                                        modele de
*                                                        pointage
*                                                        ----------
* idtyp SURFACE   12  = synop                            SURFACE
*                 13  = ship                             BATEAU
*                 14,18 = drifter                        DRIFTER
*                 131 =                                  SUPER_SYN
*                 143 = sa                               SURFACE
*                 144 = sa    automatique                SURFACE
*                 146 = synop automatique                SURFACE
*                 147 = ship  automatique                BATEAU
*                 150,151,152,153 = BOGUS                BOGUS_S
*
* idtyp UPPER AIR 12  = synop                            UPPER_AIR
*                 13  = ship                             UPPER_SHP
*                 14  = dribu                            UPPER_SHP
*                 18  = drifter                          UPPER_SHP
*                 32  = pilot                            UPPER_AIR
*                 33  = pilot ship                       UPPER_SHP
*                 35  = temp                             UPPER_AIR
*                 36  = temp ship                        UPPER_SHP
*                 37  = temp drop
*                 86  =                                  SATEM
*                 88  =                                  SATOB
*                 128 =                                  AIREP
*                 129 =                                  AIREP
*                 130 = profileur de vent
*                 131 =                                  SUPER_SYN
*                 132 =                                  SAR
*                 135 = temp + pilot                     UPPER_AIR
*                 136 = temp + SYNOP                     UPPER_AIR
*                 137 = PILOT+ SYNOP                     UPPER_AIR
*                 138 = TEMP + PILOT+ SYNOP              UPPER_AIR
*                 139 = TEMP SHIP + PILOT SHIP           UPPER_SHP
*                 140 = TEMP SHIP + SHIP                 UPPER_SHP
*                 141 = SHIP + PILOT SHIP                UPPER_SHP
*                 142 = TEMP SHIP + SHIP +PILOT SHIP     UPPER_SHP
*                 147 =                                  UPPER_SHP
*                 151,153 = BOGUS                        BOGUS_A
*
*---------------------------------------------------------------------
* les variables $btyp et $btyp2 sont definies dans les scripts
* $btyp =70 (donnees altitudes, vues par AO, global)
*      2=66 (donnees altitudes, donnees derivees entree a AO, global)
*---------------------------------------------------------------------
 PLTSEL( 'BNAT', [4,7] )
 PLTSEL( 'BTYP',$btyp )
 PLTSEL( 'BSTP', 0 )
 PLTSEL( 'IDTP', [151,153])
 PLTSEL( 'BNIV',7004, $nivplt, $nivplt, $nivplt)
 PLTBRP( 'BOGUS_A','$nom_fichier_burp')
*---------------------------------------------------------------------
 PLTSEL( 'BNAT', [0,3] )
 PLTSEL( 'BTYP',$btyp )
 PLTSEL( 'BSTP', 0 )
 PLTSEL( 'IDTP', [131])
 PLTSEL( 'BNIV',7004, $nivplt, $nivplt, $nivplt)
 PLTBRP( 'SUPER_SYN','$nom_fichier_burp')
*---------------------------------------------------------------------
 PLTSEL( 'BNAT', [4,7] )
 PLTSEL( 'BTYP',$btyp )
 PLTSEL( 'BSTP', 0 )
 PLTSEL( 'IDTP', [138,135,136,35,137,32])
 PLTSEL( 'BNIV',7004, $nivplt, $nivplt, $nivplt)
 PLTBRP( 'UPPER_AIR','$nom_fichier_burp')
*---------------------------------------------------------------------
 PLTSEL( 'BNAT', [4,7] )
 PLTSEL( 'BTYP',$btyp )
 PLTSEL( 'BSTP', 0 )
 PLTSEL( 'IDTP', [33,36,139,140,141,142])
 PLTSEL( 'BNIV',7004, $nivplt, $nivplt, $nivplt)
 PLTBRP( 'UPPER_SHP','$nom_fichier_burp')
*---------------------------------------------------------------------
 PLTSEL( 'BNAT', [0,3] )
 PLTSEL( 'BTYP',$btyp )
 PLTSEL( 'BSTP', 0 )
 PLTSEL( 'IDTP', [12])
 PLTSEL( 'BNIV',7004, $nivplt, $nivplt, $nivplt)
 PLTBRP( 'UPPER_AIR','$nom_fichier_burp')
*---------------------------------------------------------------------
 PLTSEL( 'BNAT', [0,3] )
 PLTSEL( 'BTYP',$btyp )
 PLTSEL( 'BSTP', 0 )
 PLTSEL( 'IDTP', [13,14,18,147])
 PLTSEL( 'BNIV',7004, $nivplt, $nivplt, $nivplt)
 PLTBRP( 'UPPER_SHP','$nom_fichier_burp')
*---------------------------------------------------------------------
 PLTSEL( 'BNAT', [0,3] )
 PLTSEL( 'BTYP',$btyp )
 PLTSEL( 'BSTP', 0 )
 PLTSEL( 'IDTP', 132 )
 PLTSEL( 'BNIV',7004,$nivplt, $nivplt, $nivplt)
 PLTBRP( 'SAR','$nom_fichier_burp')
*--------------------------------------------------------------------
 PLTSEL( 'BNAT', [0,3] )
 PLTSEL( 'BTYP',$btyp2 )
 PLTSEL( 'BSTP', 0 )
 PLTSEL( 'IDTP', 128)
 PLTSEL( 'BNIV', 7195, $nivplt, $nivplt, $nivplt )
 PLTBRP( 'AIREP','$nom_fichier_burp')
*---------------------------------------------------------------------
 PLTSEL( 'BNAT', [0,3] )
 PLTSEL( 'BTYP',$btyp2 )
 PLTSEL( 'BSTP', 0 )
 PLTSEL( 'IDTP', 88 )
 PLTSEL( 'BNIV', 7195, $nivplt, $nivplt, $nivplt )
 PLTBRP( 'SATOB','$nom_fichier_burp')
*---------------------------------------------------------------------
 PLTSEL( 'BNAT', [4,7] )
 PLTSEL( 'BTYP',$btyp )
 PLTSEL( 'BSTP', 0 )
 PLTSEL( 'IDTP', 86 )
 PLTSEL( 'BNIV', 7004, $nivplt, $nivplt, $nivplt )
 PLTBRP( 'SATEM','$nom_fichier_burp')
**********************************************************************
* on doit remettre les fontes car le pointage avait besoin des fontes
* de defaut (position 0) ainsi que 6-7-8-9 (symboles meteo)
*
SETFONT([15,16,10,11,12,13, 2, 3,17, 9])
*
**********************************************************************
**********************************************************************
**********************************************************************
* BOITE INFO DU COIN BAS-GAUCHE
*
 BOITE([LEFT,BOTTOM,0.10,0.10],[LEFT,BOTTOM,4.60,1.25],2,1,001)
 WRITE([LEFT,BOTTOM,  0.25,  1.05],26,
 'CMC CANADA  @STR(MUMS,A4)',["CNTR",02,"IN",2])
 WRITE([LEFT,BOTTOM,  0.25,  0.80],26,
 'ANAL ${niv} @AHP@AA',["CNTR",02,"IN",2])
 WRITE([LEFT,BOTTOM,  0.25,  0.55],26,
 '@DAT(DATS,H,Z ,DW,-,JS, ,J, ,MON,-,MOI, ,A)',["CNTR",02,"IN",3,"SIZE",175])
 WRITE([LEFT,BOTTOM,  0.25,  0.30],26,
 ' $JOBNAME_WITH_SUFFIX',["CNTR",02,"IN",2])
*
*------------------------------------------------------------------------------
*
* BOITE DE LA LEGENDE EN HAUT
* LA CARTE FAIT 28.50po, ON UTILISERA 12.75 po POUR LA LEGENDE
* CE QUI LAISSE 7.5 po DE PART ET D'AUTRE
*
*
*                  8.5          15.25
*                   !             !
*
* ---------------7.5---9.5---------------21.00-----------28.50
*                !      !                 !               !
*     -0.30      !      !                 !                     -0.15 et -0.40
*     -0.60      !      !-----------------!               -0.55
*                !      !                 !                     -0.70
*-0.85           -------!-----------------!               -0.85
*     -0.97 -1.14!      !                 !                     -0.97 et -1.14
*-1.25           -------!-----------------!               -1.25
*
*
*                   !     !         !
*                  8.5   9.70     15.45
*                        9.75     15.50
*                        9.80     15.55
*       texte:          10.00     15.75
*
*
*
*                        9.70   13.45   17.20
*  3 champs:             9.75   13.50   17.25
*                        9.80   13.55   17.30
*       texte:          10.00   13.75   17.50
*
*
 BOITE([LEFT,TOP,7.50,-1.25],[LEFT,TOP,21.00, 0.00],2,1,001)
*
*TIRE UNE LIGNE A .55 et .85
*
 BOITE([LEFT,TOP, 9.50,-0.55],[LEFT,TOP,21.00,-0.55],2,1,000)
 BOITE([LEFT,TOP, 7.50,-0.85],[LEFT,TOP,21.00,-0.85],2,1,000)
*
* LIGNE VERTICALE POUR SEPARER ANALYSE
*
 BOITE([LEFT,TOP, 9.50,-1.25],[LEFT,TOP, 9.50, 0.00],2,1,000)
*
 WRITE([LEFT,TOP, 8.50,-0.30],25, 'ANALYSIS')
 WRITE([LEFT,TOP, 8.50,-0.60],25, 'ANALYSE')
*
*
*
* HEURE DE VALIDITE
*
 WRITE([LEFT,TOP, 15.25, -0.30],25,
# '@DAT(DATV,H,Z ,DW,-,JS, ,J, ,MON,-,MOI, ,A)')
 'DATA NOT SEEN (IN BOLD)  -  DONNEES NON-VUES (EN GRAS)')
*
* DESCRIPTION DE LA CARTE
*
 WRITE([LEFT,TOP,15.25,-0.70],29,
 '${niv} @AHP@AA    HEIGHT-HAUTEUR    TEMPERATURE')
*
* EXPLICATION DU CONTENU DES CARTES
*
 BOITE([LEFT,TOP, 9.70,-1.20],[LEFT,TOP, 9.70,-0.90],4,7,000)
 BOITE([LEFT,TOP, 9.80,-1.20],[LEFT,TOP, 9.80,-0.90],2,7,000)
*
 WRITE([LEFT,TOP,10.00,-0.97],28,
 'HEIGHT - HAUTEUR',["CNTR",02, "FC",7])
 WRITE([LEFT,TOP,10.00,-1.14],28,
 '@I9 @BH @BL ...75, 78, 81... @AD@AA@AM',["CNTR",02, "FC",7])
*
*SIMULE LIGNE POINTILLE
*
 BOITE([LEFT,TOP,15.45,-1.20],[LEFT,TOP,15.45,-1.15],4,5,000)
 BOITE([LEFT,TOP,15.45,-1.09],[LEFT,TOP,15.45,-1.03],4,5,000)
 BOITE([LEFT,TOP,15.45,-0.97],[LEFT,TOP,15.45,-0.91],4,5,000)
*
 BOITE([LEFT,TOP,15.55,-1.20],[LEFT,TOP,15.55,-1.15],2,5,000)
 BOITE([LEFT,TOP,15.55,-1.09],[LEFT,TOP,15.55,-1.03],2,5,000)
 BOITE([LEFT,TOP,15.55,-0.97],[LEFT,TOP,15.55,-0.91],2,5,000)
*
 WRITE([LEFT,TOP,15.750,-0.97],28,
 'TEMPERATURE',["CNTR",02, "FC",5])
 WRITE([LEFT,TOP,15.750,-1.14],28,
 '@F+ @F-',["CNTR",02, "FC",5,"ENH",-1])
 WRITE([LEFT,TOP,15.750,-1.14],28,
 '     ... -5, 0, 5... C',["CNTR",02, "FC",5])
*
* BOITE([LEFT,TOP,18.75,-1.20],[LEFT,TOP,20.75,-0.90],1,5,002)
 BOITE([LEFT,TOP,18.75,-1.20],[LEFT,TOP,20.75,-0.90],1,5,003)
 WRITE([LEFT,TOP,19.75,-1.05],28,'-2.5@A< T @A<+2.5',
                  ["CNTR",01,"FC",5,"IN",2,"ENH","YES"])
*
* ON REFAIT LE CADRE PARCE QUE LES INDICATIFS H L PEUVENT L'AVOIR EFFACE
* 
 BOITE([LEFT,BOTTOM, 0.00,0.00],[RIGHT, BOTTOM, 0.00, 0.00],2,1,000)
 BOITE([LEFT,TOP, 0.00, 0.00],[RIGHT, TOP, 0.00, 0.00],2,1,000)
 BOITE([LEFT,BOTTOM, 0.00, 0.00],[LEFT, TOP, 0.00, 0.00],2,1,000)
 BOITE([RIGHT,BOTTOM, 0.00, 0.00],[RIGHT, TOP, 0.00, 0.00],2,1,000)
* 
**********************************************************************
 FRAME(0) 
**********************************************************************
EOFSIGMA

rm -f metacod
rm -f segfile

ln -s ${TASK_INPUT}/modelpt_nv modelpt_nv

${TASK_BIN}/sigma  -iwmodl  modelpt_nv \
        -imflds  femap \
        -i       sigmadir \
        -date    ${RUN}

rm -f femap
rm -f sigmadir
rm -f *QQQ*

${TASK_BIN}/trames -device v200 \
       -ncp 1 -colors 0 1 1 1 1 1 1 1 \
       -format rrbx \
       -dn rrbx \
       -orn rot -size 3000 -hx 2850 -hy 2100 -offset 0

if [ ${RUNHOUR} =  "00" ] ; then
   ${TASK_BIN}/ocxcarte -f plhbx00 -d plot -r ${SEQ_SHORT_DATE}
fi
if [ ${RUNHOUR} =  "06" ] ; then
   ${TASK_BIN}/ocxcarte -f plhbx06 -d plot -r ${SEQ_SHORT_DATE}
fi
if [ ${RUNHOUR} =  "12" ] ; then
   ${TASK_BIN}/ocxcarte -f plhbx12 -d plot -r ${SEQ_SHORT_DATE} 
fi
if [ ${RUNHOUR} =  "18" ] ; then
   ${TASK_BIN}/ocxcarte -f plhbx18 -d plot -r ${SEQ_SHORT_DATE} 
fi


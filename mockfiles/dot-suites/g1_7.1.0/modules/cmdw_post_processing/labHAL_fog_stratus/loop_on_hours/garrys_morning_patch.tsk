#!/usr/bin/ksh

getFileTimeExtension()
{
echo  "function getFileTimeExtension"
#For model where hours are groups in one file .
#Example for eps files 0-24 forecast are gouped in the 024 file
hour=$1
fileTimeFrequency=$2


if [ "${fileTimeFrequency}" = "" -o  "${fileTimeFrequency}" = "1" ]
then
  EXT=$(printf %03d $hour)
else
  modulo=$(( ${hour} % ${fileTimeFrequency} ))
  if [ $modulo -eq 0 ]
  then
    H=${hour}
  else
    H=$((  ((${hour}/${fileTimeFrequency}) + 1)*${fileTimeFrequency} ))
  fi
  EXT=`printf %03d $H`
fi
}
#Special case for 1st forecast (01h)
zapFogToGarrysMorningPatch()
{
current_hour_loop=${FOG_STRATUS_forecastLast}
fog_previous_hour=${TASK_WORK}/../../fog_stratus_calculate+${current_hour_loop}/work/fog_001.std
garrys_patch_current_hour=${TASK_WORK}/${FOG_STRATUS_yyyymmddhh}_001${_MMM}

${TASK_BIN}/EDITFST -e -s ${fog_previous_hour} -d ${garrys_patch_current_hour} -i <<FINEDIT
 desire(-1,'COM0',-1,-1,-1,1,-1)
 zap(-1,'FST',-1,-1,-1,-1,-1)
FINEDIT

${TASK_BIN}/EDITFST -e -s ${fog_previous_hour} -d ${garrys_patch_current_hour} -i <<FINEDIT
 desire(-1,'^^')
 desire(-1,'>>')
FINEDIT
}

calculateGarrysMorningPatch()
{

#Get file hour extension for this forecast time
getFileTimeExtension ${forecastTime} ${FOG_STRATUS_fileTimeExtensionFrequency}
HHH=$EXT

#Get member extension . Empty string for determinist
if [ "${FOG_STRATUS_member}" = "" ]
then
  _MMM=""
else
  _MMM=_$(printf %03d ${FOG_STRATUS_member})
fi


forecastTimeMinus1h=$((forecastTime-1))
getFileTimeExtension ${forecastTimeMinus1h} ${FOG_STRATUS_fileTimeExtensionFrequency}
HHHM1=$EXT

current_hour_loop=${FOG_STRATUS_forecastLast}

#Check if its first forecast hour in loop. 
#If so, it means that the previous hour file was in the previous loop

if [ ${forecastTime} -eq ${FOG_STRATUS_forecastFirst} ] 
then
   previous_hour_loop=$((current_hour_loop-${FOG_STRATUS_nHour_in_loop}))
else
   previous_hour_loop=${current_hour_loop}
fi

fog_previous_hour=${TASK_WORK}/../../fog_stratus_calculate+${previous_hour_loop}/work/fog_$(printf %03d ${forecastTimeMinus1h}).std
fog_current_hour=${TASK_WORK}/../../fog_stratus_calculate+${current_hour_loop}/work/fog_$(printf %03d ${forecastTime}).std

cx_current_hour=${TASK_INPUT}/${FOG_STRATUS_cxDir}/${FOG_STRATUS_yyyymmddhh}_${HHH}${_MMM}
cx_previous_hour=${TASK_INPUT}/${FOG_STRATUS_cxDir}/${FOG_STRATUS_yyyymmddhh}_${HHHM1}${_MMM}

garrys_patch_previous_hour=${TASK_WORK}/../../garrys_morning_patch+${previous_hour_loop}/work/${FOG_STRATUS_yyyymmddhh}_${HHHM1}${_MMM}
garrys_patch_current_hour=${TASK_WORK}/${FOG_STRATUS_yyyymmddhh}_${HHH}${_MMM}

for thisInFile in ${fog_previous_hour} ${fog_current_hour} ${garrys_patch_previous_hour} ${cx_current_hour} ${cx_previous_hour}
do
  if [ ! -f $thisInFile ]
  then
    echo "TASK ERROR AT FORECASTIME $forecastTime INPUT FILE [$thisInFile] IS MISSING"
    echo "MUST EXIT :-("
    ${SEQ_BIN}/nodelogger -n ${SEQ_NODE} -s abort -m "ERROR: AT FORECASTIME $forecastTime INPUT FILE [$thisInFile] IS MISSING"
    exit 1
  fi
done

#Remove links from previous hour
cmcDate=$(${TASK_BIN}/r.date -S ${FOG_STRATUS_yyyymmddhh} +${forecastTime})
cmcDateMinus1h=$(${TASK_BIN}/r.date -S ${cmcDate} -1)
cx_current_hour=${TASK_INPUT}/${FOG_STRATUS_cxDir}/${FOG_STRATUS_yyyymmddhh}_${HHH}${_MMM}
cx_previous_hour=${TASK_INPUT}/${FOG_STRATUS_cxDir}/${FOG_STRATUS_yyyymmddhh}_${HHHM1}${_MMM}

#2DFile must contain the following input variables:
#previous hour: CX and  FST. 
#this hour: COM0 TODO CX RHML RB
#FST is output from previous hour garry_morning_patch 
#COM0, TODO, RHML, RB are outputs from calculate_fog task
#CX is from model
#output field is FST for this hour. This is the final field that will
#be sent to diag

${TASK_BIN}/GPATCHEXE -2DFile ${fog_previous_hour}:${fog_current_hour}:${garrys_patch_previous_hour}:${cx_current_hour}:${cx_previous_hour} \
                      -cmcDate $cmcDate \
                      -cmcDateMinus1h $cmcDateMinus1h \
                      -outFile ${garrys_patch_current_hour} 

${TASK_BIN}/EDITFST -e -s ${fog_current_hour} -d ${TASK_WORK}/${FOG_STRATUS_yyyymmddhh}_${HHH}${_MMM} -i <<FINEDIT
 desire(-1,'^^',-1,-1,-1,-1,-1)
 desire(-1,'>>',-1,-1,-1,-1,-1)
FINEDIT


}

Send2Diag()
{
echo "send to diag"
for f in $(ls ${TASK_WORK}/${FOG_STRATUS_yyyymmddhh}_???${_MMM})
do
  ${TASK_BIN}/diag.writer -f  ${f} -d ${TASK_OUTPUT}/diagDir/$(basename $f)
done

}



#------------------------------------------------
# M A I N    P R O G R A M
#
# Original script  Curtis Mooney Bill Burrows, HAL
# 
# Modifications: Ronald 2017-03-11
#                Adapted for maestro
#                Added possibility for eps files   
#            
# Important: A dependency to fog_stratus need to be configurated
#            for this task. fog_stratus need to be completed for all hour previous
#            to this forecast before starting this task. Also, this task need to be done 
#            one hour at a time in sequence.
# 
#------------------------------------------------

FOG_STRATUS_forecastFirst=$(( FOG_STRATUS_forecastLast - FOG_STRATUS_nHour_in_loop + 1 ))

if [ "${FOG_STRATUS_cxDir}" = "" ]
then
  FOG_STRATUS_cxDir=etaDir
fi

for forecastTime in $(seq ${FOG_STRATUS_forecastFirst} ${FOG_STRATUS_forecastLast})
do
  if [ $forecastTime -eq 1 ]
  then
    zapFogToGarrysMorningPatch
  else 
    calculateGarrysMorningPatch
  fi
done

Send2Diag

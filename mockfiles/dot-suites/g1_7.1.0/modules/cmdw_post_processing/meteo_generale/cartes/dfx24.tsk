#!/bin/ksh
#***
#.*****************************************************************************
#.
#.     JOB NAME - g1dfx24_y2k
#.
#.     STATUS - OPERATIONAL - SEMI-ESSENTIAL
#.
#.     ORIGINATOR - Gilles Desautels
#.
#.                  REVISION:   1.0 (original) - 09 avril 1992 
#.                              1.1 - 03 novembre 1992 passe de R2 a G1
#.
#.     PRODUCT - difax chart
#.     
#.               metsis_#   cmc_#    time avbl(GMT)   description 
#.               --------   ------   --------------   -----------
#.     
#.     cmc_dfx   A0529C     ca0529c     0435          global 2 panel 850mb
#.     cmc_dfx   A0609C     ca0609c     1635          global 2 panel 850mb
#.     
#.
#.     DESCRIPTION - 850mb 12hr and 24hr temperatures and heights. 
#.
#.     Revision:  21 dec 98 - an 2000
#                 utilise des utilitaires RPN seulement
#                 aucun changement pour Y2K
#.
#.                Decembre 2013 - Naysan Saran
#.                Conversion a Maestro
#.
#.*****************************************************************************

# nom standard du fichier pgsm
nom_fichier_pgsm=${TASK_INPUT}/fichier_pgsm/*
echo $nom_fichier_pgsm

###############################################################################
#  pgsm va chercher les champs dans les projections desirees
#  les cartes de controles pour pgsm

cat << EOFPGSM > pgsmdir
#
#
*  JOB NAME -   g1dfx24
* CARTES PREVUES DU MODELE GLOBAL
* 2 PANNEAUX AU NIVEAU 850 MB aux heures 12-24
*******************************************************************************
* 30 JANV 1992
* GILLES DESAUTELS (DDO)
*******************************************************************************
* ICI ON UTILISE RESOLUTION DE 76.2 KM
*
 SORTIE(STD,30,A)
*
* pour eliminer les problemes causes par les extrapolations
*
 EXTRAP(VOISIN)
*
 GRILLE(PS,191,141, 101.0, 101.0,  76200., 21.0, NORD)
*
 COMPAC=-16 
*
 HEURE(12,24)
*
 CHAMP('GZ',850)
 CHAMP('TT',850)
*
*------------------------------------------------------------------------------
*                                                                     FIN  PGSM
*------------------------------------------------------------------------------
EOFPGSM
#
#  execution du pgsm
#
rm -f femap
rm -f femap
${TASK_BIN}/pgsm -iment $nom_fichier_pgsm \
         -ozsrt femap \
         -i pgsmdir

###############################################################################
cat << EOFSIGMA > sigmadir
#
# 
* DIRECTIVES SIGMA POUR PRODUIRE R0DFX24
* carte 2 panneaux du global - carte 850mb aux heures 12-24
* on procede panneau apres panneau
*******************************************************************************
* 30 janv 1992
* GILLES DESAUTELS (DDO)
*******************************************************************************
* CARTES SUR VERSATEC
*--------------------
* ON TRACE ICI 2 PANNEAUX COTE A COTE EN UTILISANT L'ECHELLE 1:40M
* CHACUN AYANT LA DIMENSION SUIVANTE * ICI ON UTILISE RESOLUTION DE 76.2 KM
*
*        14.25 X 10.50
*
*
* COMME LA SECONDE CARTE COMMENCE A 10.50 PO SELON Y
*                                   14.25          X
* dans trames on fait comme si on avait 4 panneaux
* ON UTILISE DANS TRAMES HY=2100, HX=1425+1425=2850, ORN=ROT
*******************************************************************************
*
*                DIRECTIVES SIGMA
*
*                DIRECTIVES GLOBALES
*
*******************************************************************************
 CLIP = OUI
 NORMID = NON
 LISSAGE=OUI
*
*LA FONTE 15 CONTIENT LES MAJUSCULES, LA 16 LES MINUSCULES
*LA FONTE 10 CONTIENT LES LETTRES MAJUSCULES DES H,L
*LA FONTE 11 CONTIENT LES LETTRES MINUSCULES CORRESPONDANTES
*LA FONTE 17 CONTIENT LES CHIFFRES DES ETIQUETTES (FONTE ETROITE)
*LA FONTE  2 CONTIENT LES GRANDS + ET -
*LA FONTE  3 CONTIENT LES GRANDS LETTRES GRECQUES
*LA FONTE  9 CONTIENT LE CENTRE (X DANS O)
*
*   @         A  B  C  D  E  F  G  H  I
*   "CASE" 0  1  2  3  4  5  6  7  8  9
 SETFONT([15,16,10,11,12,13, 2, 3,17, 9])
*
* OPTIONS GEOGRAPHIQUES
*
 MAPLAB="ST",1,1,1,1,10,2,NON,OUI,NON,1,.5,OUI
 MAPCOLR=1
 OPTIONS(MAPOPT,["OU","GL","GR",-1.0,"LA","NO"]) 
 OPTIONS(DASHSET,["SMALL",16.0,"NP",1500])
*
 ECHELLE=40.0 
 POUCES=30.0
*
 ECHLGRI=NON
*
 OPTIONS(ISPSET,["NCRT",2])
*
* SETHPAL NECESSAIRE POUR PRODUIRE EFFACEMENT AVEC PATRON 1
*                         COLORIER LE MOTIF 2 (=099) (VERT - TRANSPARENT)
*                         COLORIER LE MOTIF 3 (=109) (VERT - TRANSPARENT)
*                         COLORIER LE MOTIF 4 (=099) (VIOLET-TRANSPARENT)
*                         COLORIER LE MOTIF 5 (=109) (VIOLET-TRANSPARENT)
*                         COLORIER LE MOTIF 6 (=113) (VERT - TRANSPARENT)
*
*               1       2       3       4       5       6
 SETHPAL([0001001,0616099,0616109,0516099,0516109,0616113],6)
*
************************************************************************........
*                    GROUPE
************************************************************************........
* 
* GROUPE LOT=6-7   POUR LES ISOHYPSES DES HAUTEURS     6-majeur (TH=4)
*                  COULEUR CO=7 (BLEU)                 7-mineur (TH=2)
*                  BOITE FOND NOIR
*
 GROUPE(6,["CO",7,"TH",4,"CA",8,
           "FC",0,"BC",7,"CF","YES",
           "IN",1,"SI",175,"CN",34])
 GROUPE(7,["CO",7,"TH",2,"CA",8,
           "FC",0,"BC",7,"CF","YES",
           "IN",1,"SI",175,"CN",34])
*
* GROUPE LOT=8-9-10 POUR LA TAILLE DES MAX-MIN DES HAUTEURS  (H L)
* a noter in=3 pour avoir un H L completement plein
* a noter si=80 pour le centre, ce qui donne un rond plein (et petit)
*
 GROUPE( 8,["SI",600,"IN",3,"FC",7,"ENH","YES","CN",01]) 
 GROUPE( 9,["SI",125,"IN",2,"FC",7,"ENH","YES","CN",01]) 
 GROUPE(10,["SI",225,"IN",2,"FC",7,"ENH","YES","CN",01,"CA",8]) 
*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
* 
* GROUPE LOT=16-17  POUR DES ISOHYPSES DES TEMPERATURES  16-majeur (TH=3)
*                   COULEUR CO=5 (VIOLET)                17-mineur (TH=2)
*                   BOITE FOND BLANC
*
 GROUPE(16,["CO",5,"TH",3,"CA",8,
            "FC",5,"BOX","YES","CLR","YES",
            "IN",1,"SI",175,"CN",34])
 GROUPE(17,["CO",5,"TH",2,"CA",8,
            "FC",5, "BOX","YES","CLR","YES",
            "IN",1,"SI",175,"CN",34])
*
* GROUPE LOT=18-19-20 POUR LA TAILLE DES MAX-MIN DES TEMPERATURES (+ -)
* defini precedemment (meme que pour les epaisseurs)
*
 GROUPE(18,["SI",600,"IN",3,"FC",5,"ENH","YES","CN",01]) 
 GROUPE(19,["SI",125,"IN",2,"FC",5,"ENH","YES","CN",01]) 
 GROUPE(20,["SI",225,"IN",2,"FC",5,"ENH","YES","CN",01,"CA",8]) 
*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
*
* ECRITURE ID CARTE
*
* GROUPE 25 POUR TITRE GLOBAL
* GROUPE 26 POUR ID DU CMC ET DESCRIPTION DU CONTENU (BOITE NORMALE)
*        27      HEURE DE VALIDITE et ecriture dans les zones ombragees
*        28      CONTENU DESCRIPTIF
*        29      TITRE DE LA CARTE
*
* "CA",0 => FONTE 15  (defaut)
* "CA",8 => FONTE  8  (etroit)
*
 GROUPE(25,["CA",0,"SI",400,"CW",225,"IN",2,"FC",1,"CN",01])
 GROUPE(26,["CA",0,"SI",100,"IN",1,"FC",1,"CN",01])
 GROUPE(27,["CA",0,"SI",110,"IN",1,"FC",1,"CN",01])
 GROUPE(28,["CA",0,"SI",100,"IN",1,"FC",1,"CN",01])
 GROUPE(29,["CA",0,"SI",175,"IN",2,"FC",1,"CN",01])
*
*------------------------------------------------------------------------------
* ID DU CMC ET HEURE DE VALIDITE A GAUCHE
*
 MUMS(["529","529","609","609"])
*
*------------------------------------------------------------------------------
*
************************************************************************
******************************************************* BAS-GAUCHE *****
******************************************************* 850mb 24h  *****
************************************************************************
 PANNEAU([0.0, 0.0]) 
*
* OFFM FAIT QUE LE MESSAGE EN BAS DU GRAPHIQUE EST ELIMINE
* MXVA ET MYVA PERMET D'ELIMINER LES MAX-MIN TROP RAPPROCHES
* ON PEUT FAIRE VARIER CES NOMBRES SELON LE BRUIT DU CHAMP
*                                        et la distance de grille
*
* POUR UNE CARTE 1:40M distance = 0.075 po 
*      donc la distance minimum entre 2 H ou 2 L sera de
*       8 * .075 po = 0.60 po  si MXVA,MYVA=4
*      12 * .075 po = 0.90 po  si MXVA,MYVA=6
*
* OPTIONS(ISPSET,["OFFM",1, "MXVA",6, "MYVA",6])
************************************************************************........
*
 SCAL ("TT",   1.0, 5.0)
 LIMITE=-70., +70.
 DASH([0525B])
*
 NIVEAUX = -2.5, +2.5
* MOTIFS =000,004,000
* HAFTON("TT","P",-1, 850,24, 0,[GEO,NIV,PAT])
*
 MOTIFS =000,005,000
 HAFTON("TT","P",-1, 850,24, 0,[GEO,NIV,PAT])
*
 OPTIONS(ISPSET,["RNDL",1., "RNDV",1.])
 OPTIONS(ISPSET,["ECOL",350650950, "LABL",16, "ILAB",1])
 OPTIONS(ISPSET,["MAJR",16, "MINR",17, "NULB",13])
*
*DIRECTIVES POUR LES MAX-MIN DES TEMPERATURES
*
 OPTIONS(ISPSET,["HCAR","@F+","LCAR","@F-","CCAR"," ","LHCE",2,"NHL",3])
 OPTIONS(ISPSET,["HILO",19,"CNTR",19,"VALU",20])
 OPTIONS(ISPSET,["OFFM",1, "MXVA",6, "MYVA",6])
*
 VARIAN("TT","P",-1, 850, 24, 0,[GEO])
*
 SCAL ("GZ",   1.0, 6.0)
 LIMITE= 54.,  246.
 DASH([1777B])
*
 OPTIONS(ISPSET,["RNDL",1., "RNDV",1.])
 OPTIONS(ISPSET,["ECOL",250550850, "LABL",6, "ILAB",1])
 OPTIONS(ISPSET,["MAJR",6, "MINR",7, "NULB",3])
*
*DIRECTIVES POUR LES MAX-MIN DES HAUTEURS
*
 OPTIONS(ISPSET,["HCAR","@BH","LCAR","@BL","CCAR","@I9","LHCE",1,"NHL",4])
 OPTIONS(ISPSET,["HILO", 8,"CNTR", 9,"VALU",10])
 OPTIONS(ISPSET,["OFFM",1, "MXVA",6, "MYVA",6])
*
 VARIAN("GZ","P",-1,850,24, 0,[GEO,PAN])
*
*******************************************************************************
* BOITE DE LA LEGENDE EN HAUT
* LA CARTE FAIT 14.25po, ON UTILISERA 14.25 po POUR LA LEGENDE
*
*                  1.0           8.12
*                   !             !
*
*                0.0---2.0------------14.25
*                !      !                 !             
*     -0.25      !      !                 !                     -0.15 et -0.40
*     -0.65      !      !-----------------!               -0.55
*                !      !                 !                     -0.70
*-0.85           -------!-----------------!               -0.85
*     -0.97 -1.14!      !                 !                     -0.97 et -1.14
*-1.25           -------!-----------------!               -1.25
*
*
*                   !     !         !
*                  1.0   2.20      8.20
*                        2.25      8.25
*                        2.30      8.30
*       texte:           2.50      8.50
*
*
*
*                        2.20    6.20   10.20
*  3 champs:             2.25    6.25   10.25
*                        2.30    6.30   10.30
*       texte:           2.50    6.50   10.50
*******************************************************************************
* ZONE POUR LA LEGENDE (on efface ce qui est deja trace)
*                      (devra se faire apres le contourage
*                       lorsque la fenetre du global
*                       couvrira la zone entiere puisqu'on
*                       doit effacer ce qui est deja la)
*
 BOITE([LEFT,TOP, 0.0,-1.25],[LEFT,TOP,14.25, 0.00],2,1,001)
*
*TIRE UNE LIGNE A .55 et .85
*
 LIGNE([LEFT,TOP, 2.00,-0.55],[LEFT,TOP,14.25,-0.55],2,1)
 LIGNE([LEFT,TOP, 0.00,-0.85],[LEFT,TOP,14.25,-0.85],2,1)
*
* LIGNE VERTICALE POUR SEPARER GLOBAL
*
 LIGNE([LEFT,TOP, 2.00,-1.25],[LEFT,TOP, 2.00, 0.00],2,1)
*
 WRITE([LEFT,TOP, 1.00,-0.45],25, 'GLOBAL')
*  WRITE([LEFT,TOP, 1.00,-0.97],28, 'CUTOFF-TOMBEE')
*  WRITE([LEFT,TOP, 1.00,-1.14],28, '$JOBNAME_WITH_SUFFIX')
*
* HEURE DE VALIDITE
*
 WRITE([LEFT,TOP,  8.12, -0.40],29,
 'V@DAT(DATV,H,Z ,DW,-,JS, ,J, ,MON,-,MOI, ,A)',["IN",2])
*
* DESCRIPTION DE LA CARTE
*
 WRITE([LEFT,TOP, 8.12,-0.15],29,
 '24H FORECAST - PREVISION 24@AH', ["CNTR",01])
 WRITE([LEFT,TOP, 8.12,-0.70],29,
 '850 @AHP@AA HEIGHT-HAUTEUR   TEMPERATURE', ["CNTR",01])
*
* EXPLICATION DU CONTENU DES CARTES
* hauteur a 850mb
*
* ligne verticale epaisse et mince
*
 LIGNE([LEFT,TOP, 2.20,-1.20 ],[LEFT,TOP,2.20,-0.90 ],2,7)
 LIGNE([LEFT,TOP, 2.30,-1.20 ],[LEFT,TOP,2.30,-0.90 ],1,7)
*
 WRITE([LEFT,TOP, 2.50,-0.97 ],28, 'HEIGHT - HAUTEUR',
 ["CNTR",02, "FC",7])
 WRITE([LEFT,TOP, 2.50,-1.14],28,
 '@I9 @BH @BL ...144, 150, 156... @AD@AA@AM',["CNTR",02, "FC",7])
*
*
*SIMULE LIGNE POINTILLE (pour temperatures couleur=violet=5)
*
 LIGNE([LEFT,TOP, 8.20,-1.20],[LEFT,TOP, 8.20,-1.15],2,5)
 LIGNE([LEFT,TOP, 8.20,-1.09],[LEFT,TOP, 8.20,-1.03],2,5)
 LIGNE([LEFT,TOP, 8.20,-0.97],[LEFT,TOP, 8.20,-0.91],2,5)
*
 LIGNE([LEFT,TOP, 8.30,-1.20],[LEFT,TOP, 8.30,-1.15],1,5)
 LIGNE([LEFT,TOP, 8.30,-1.09],[LEFT,TOP, 8.30,-1.03],1,5)
 LIGNE([LEFT,TOP, 8.30,-0.97],[LEFT,TOP, 8.30,-0.91],1,5)
*
 WRITE([LEFT,TOP, 8.50,-0.97],28,
 'TEMPERATURE',["CNTR",02, "FC",5])
 WRITE([LEFT,TOP, 8.50,-1.14],28,
 '@F+ @F- ...-5, 0, 5... C',["CNTR",02, "FC",5])
*
* BOITE([LEFT,TOP,12.00,-1.20],[LEFT,TOP,14.00,-0.90],1,5,004)
 BOITE([LEFT,TOP,12.00,-1.20],[LEFT,TOP,14.00,-0.90],1,5,005)
 WRITE([LEFT,TOP,13.00,-1.05],27,'-2.5@A< T @A<+2.5',
                  ["CNTR",01,"FC",5,"IN",2,"ENH","YES"])
*
*------------------------------------------------------------------------------
* BOITE INFO DU COIN BAS-GAUCHE  (version reduite) (imprime 1 seule fois)
*
 BOITE([LEFT,BOTTOM,0.05,0.05],[LEFT,BOTTOM,3.0,1.20],2,1,001)
*
 WRITE([LEFT,BOTTOM,  0.15,  0.50],26,
 '850 @AHP@AA GLOBAL',["CNTR",12,"IN",2])
 WRITE([LEFT,BOTTOM,  0.15,  0.30],26,
 '@DAT(DATS,H,Z ,DW,-,JS, ,J, ,MON,-,MOI, ,A)',["CNTR",12,"IN",2])
 WRITE([LEFT,BOTTOM,  0.15,  0.10],26,
 ' $JOBNAME_WITH_SUFFIX          @STR(MUMS,A3)',["CNTR",12,"IN",2])
*
************************************************************************
*
* ON REFAIT LE CADRE PARCE QUE LES INDICATIFS H L PEUVENT L'AVOIR EFFACE
*
 LIGNE([LEFT,BOTTOM, 0.00, 0.0],[RIGHT, BOTTOM, 0.00, 0.00],2,1)
 LIGNE([LEFT,TOP, 0.00, 0.00],[RIGHT, TOP, 0.00, 0.00],2,1)
 LIGNE([LEFT,BOTTOM, 0.00, 0.00],[LEFT, TOP, 0.00, 0.00],2,1)
 LIGNE([RIGHT,BOTTOM, 0.00, 0.00],[RIGHT, TOP, 0.00, 0.000],2,1)
*
************************************************************************
******************************************************* HAUT-GAUCHE*****
******************************************************* 850mb  12  *****
************************************************************************
 PANNEAU([ 0.00,10.5]) 
*
* OFFM FAIT QUE LE MESSAGE EN BAS DU GRAPHIQUE EST ELIMINE
* MXVA ET MYVA PERMET D'ELIMINER LES MAX-MIN TROP RAPPROCHES
* ON PEUT FAIRE VARIER CES NOMBRES SELON LE BRUIT DU CHAMP
*                                        et la distance de grille
*
* POUR UNE CARTE 1:40M distance = 0.075 po 
*      donc la distance minimum entre 2 H ou 2 L sera de
*       8 * .075 po = 0.60 po  si MXVA,MYVA=4
*      12 * .075 po = 0.90 po  si MXVA,MYVA=6
*
* OPTIONS(ISPSET,["OFFM",1, "MXVA",6, "MYVA",6])
************************************************************************........
*
* SCAL ("TT",   1.0, 5.0)
 LIMITE=-70., +70.
 DASH([0525B])
*
 NIVEAUX = -2.5, +2.5
* MOTIFS =000,004,000
* HAFTON("TT","P",-1, 850,12, 0,[GEO,NIV,PAT])
*
 MOTIFS =000,005,000
 HAFTON("TT","P",-1, 850,12, 0,[GEO,NIV,PAT])
*
 OPTIONS(ISPSET,["RNDL",1., "RNDV",1.])
 OPTIONS(ISPSET,["ECOL",350650950, "LABL",16, "ILAB",1])
 OPTIONS(ISPSET,["MAJR",16, "MINR",17, "NULB",13])
*
*DIRECTIVES POUR LES MAX-MIN DES TEMPERATURES
*
 OPTIONS(ISPSET,["HCAR","@F+","LCAR","@F-","CCAR"," ","LHCE",2,"NHL",3])
 OPTIONS(ISPSET,["HILO",19,"CNTR",19,"VALU",20])
 OPTIONS(ISPSET,["OFFM",1, "MXVA",6, "MYVA",6])
*
 VARIAN("TT","P",-1, 850, 12, 0,[GEO])
*
*
* SCAL ("GZ",   1.0, 6.0)
 LIMITE= 54.,  246.
 DASH([1777B])
*
 OPTIONS(ISPSET,["RNDL",1., "RNDV",1.])
 OPTIONS(ISPSET,["ECOL",250550850, "LABL",6, "ILAB",1])
 OPTIONS(ISPSET,["MAJR",6, "MINR",7, "NULB",3])
*
*DIRECTIVES POUR LES MAX-MIN DES PRESSIONS PNM
*
 OPTIONS(ISPSET,["HCAR","@BH","LCAR","@BL","CCAR","@I9","LHCE",1,"NHL",4])
 OPTIONS(ISPSET,["HILO", 8,"CNTR", 9,"VALU",10])
 OPTIONS(ISPSET,["OFFM",1, "MXVA",6, "MYVA",6])
*
 VARIAN("GZ","P",-1,850,12, 0,[GEO,PAN])
*
*******************************************************************************
* ZONE POUR LA LEGENDE (on efface ce qui est deja trace)
*                      (devra se faire apres le contourage
*                       lorsque la fenetre du global
*                       couvrira la zone entiere puisqu'on
*                       doit effacer ce qui est deja la)
*
 BOITE([LEFT,TOP, 0.0,-1.25],[LEFT,TOP,14.25, 0.00],2,1,001)
*
*TIRE UNE LIGNE A .55 et .85
*
 LIGNE([LEFT,TOP, 2.00,-0.55],[LEFT,TOP,14.25,-0.55],2,1)
 LIGNE([LEFT,TOP, 0.00,-0.85],[LEFT,TOP,14.25,-0.85],2,1)
*
* LIGNE VERTICALE POUR SEPARER GLOBAL
*
 LIGNE([LEFT,TOP, 2.00,-1.25],[LEFT,TOP, 2.00, 0.00],2,1)
*
 WRITE([LEFT,TOP, 1.00,-0.45],25, 'GLOBAL')
*  WRITE([LEFT,TOP, 1.00,-0.97],28, 'CUTOFF-TOMBEE')
*  WRITE([LEFT,TOP, 1.00,-1.14],28, '$JOBNAME_WITH_SUFFIX')
*
* HEURE DE VALIDITE
*
 WRITE([LEFT,TOP,  8.12, -0.40],29,
 'V@DAT(DATV,H,Z ,DW,-,JS, ,J, ,MON,-,MOI, ,A)',["IN",2])
*
* DESCRIPTION DE LA CARTE
*
 WRITE([LEFT,TOP, 8.12,-0.15],29,
 '12H FORECAST - PREVISION 12@AH', ["CNTR",01])
 WRITE([LEFT,TOP, 8.12,-0.70],29,
 '850 @AHP@AA HEIGHT-HAUTEUR   TEMPERATURE', ["CNTR",01])
*
* EXPLICATION DU CONTENU DES CARTES
* hauteur a 850mb
*
* ligne verticale epaisse et mince
*
 LIGNE([LEFT,TOP, 2.20,-1.20 ],[LEFT,TOP,2.20,-0.90 ],2,7)
 LIGNE([LEFT,TOP, 2.30,-1.20 ],[LEFT,TOP,2.30,-0.90 ],1,7)
*
 WRITE([LEFT,TOP, 2.50,-0.97 ],28, 'HEIGHT - HAUTEUR',
 ["CNTR",02, "FC",7])
 WRITE([LEFT,TOP, 2.50,-1.14],28,
 '@I9 @BH @BL ...144, 150, 156... @AD@AA@AM',["CNTR",02, "FC",7])
*
*
*SIMULE LIGNE POINTILLE (pour temperatures couleur=violet=5)
*
 LIGNE([LEFT,TOP, 8.20,-1.20],[LEFT,TOP, 8.20,-1.15],2,5)
 LIGNE([LEFT,TOP, 8.20,-1.09],[LEFT,TOP, 8.20,-1.03],2,5)
 LIGNE([LEFT,TOP, 8.20,-0.97],[LEFT,TOP, 8.20,-0.91],2,5)
*
 LIGNE([LEFT,TOP, 8.30,-1.20],[LEFT,TOP, 8.30,-1.15],1,5)
 LIGNE([LEFT,TOP, 8.30,-1.09],[LEFT,TOP, 8.30,-1.03],1,5)
 LIGNE([LEFT,TOP, 8.30,-0.97],[LEFT,TOP, 8.30,-0.91],1,5)
*
 WRITE([LEFT,TOP, 8.50,-0.97],28,
 'TEMPERATURE',["CNTR",02, "FC",5])
 WRITE([LEFT,TOP, 8.50,-1.14],28,
 '@F+ @F- ...-5, 0, 5... C',["CNTR",02, "FC",5])
*
* BOITE([LEFT,TOP,12.00,-1.20],[LEFT,TOP,14.00,-0.90],1,5,004)
 BOITE([LEFT,TOP,12.00,-1.20],[LEFT,TOP,14.00,-0.90],1,5,005)
 WRITE([LEFT,TOP,13.00,-1.05],27,'-2.5@A< T @A<+2.5',
                  ["CNTR",01,"FC",5,"IN",2,"ENH","YES"])
*
************************************************************************
*
* ON REFAIT LE CADRE PARCE QUE LES INDICATIFS H L PEUVENT L'AVOIR EFFACE
*
 LIGNE([LEFT,BOTTOM, 0.00, 0.0],[RIGHT, BOTTOM, 0.00, 0.00],2,1)
 LIGNE([LEFT,TOP, 0.00, 0.00],[RIGHT, TOP, 0.00, 0.00],2,1)
 LIGNE([LEFT,BOTTOM, 0.00, 0.00],[LEFT, TOP, 0.00, 0.00],2,1)
 LIGNE([RIGHT,BOTTOM, 0.00, 0.00],[RIGHT, TOP, 0.00, 0.000],2,1)
*
**********************************************************************
* il faut utiliser la meme description de fontes que celle du
* programme fortran drapeau.f qui a genere le segment 'DRAPO'
* a savoir: DATA NEWFONT/15,16,2,3,4,5,6,7,8,9/
*
 SETFONT([15,16, 2, 3, 4, 5, 6, 7, 8, 9])
*
*
* superposition du segment avec le drapeau
*
* segment(nom du segment,
*         coordonnees du point pivot dans le frame,    entier=unite normalise
*                                                      reel = coordonnees utilisateur
*         facteur echelle NUMERATEUR/DENOMINATEUR,     2 entiers
*         rotation en degres,                          entiers
*         point pivot dans l espace segment            entiers, coordonnees pseudo
*
* cet appel donne de tres grosses lettres
*  SEGMENT ('DRAPO', 1, 1, 1, 1, 0, 0, 0)
* cet appel est de bonne dimension mais mal place
*  SEGMENT ('DRAPO', 1, 1, 1, 4, 0, 0, 0)
*
* quand on passe a des coordonnees reelles
* on a un resultat dans le coin inferieur gauche
* du panneau superieur droit
* et meme la directive panneau suivante ne corrige pas
* la situation
*       PANNEAU([0.0, 0.0])
*
* On semble donc etre oblige de travailler en coordonnees entieres
*
  SEGMENT ('DRAPO',140,625, 2, 9, 0, 0, 0)
*
**********************************************************************
 FRAME(0) 
*
EOFSIGMA

rm -f metacod
rm -f segfile

cp ${TASK_INPUT}/segfile segfile

${TASK_BIN}/sigma  -imflds femap \
       -i sigmadir \
       -date ${RUN}

${TASK_BIN}/trames -device difax \
       -format rrbx \
       -ncp 1 -colors 0 1 1 1 1 1 1 1 \
       -dn rrbx \
       -orn rot -size 2660 -offset 0

if [ $RUNHOUR =  "00" ] ; then
    ${TASK_BIN}/ocxcarte -t -f ca0529c -d difax -r ${SEQ_SHORT_DATE}
fi
if [ $RUNHOUR =  "12" ] ; then
    ${TASK_BIN}/ocxcarte -t -f ca0609c -d difax -r ${SEQ_SHORT_DATE}
fi


#!/bin/ksh
#CMOI_LEVEL op
#CMOI_PLATFORM op_f
###############################################################################
# REVISION 2.0   certification An 2000
#                Gilles Desautels  27 aout 98
###############################################################################
#JOBNAME:    script_pcpn hh debut fin
#            (maximum de 15 jours avec ce script)
#
#   (removed 2006/03)  ->  hh    = heure de depart dans G1hh  (fixe a 00)
#			   debut = rang du jour de debut(1,2,...)
#                          fin   = rang du jour de la fin
#
#            calcul des accumulations de pcpn
#            le jour 1 fait la difference 24 - 0
#                    2                    48 - 24
#
#AUTHOR:     Gilles Desautels
#
#OBJECT:     contourage de champs du total de precipitations dans une periode 
#            de plusieurs jours
#
#            sortie operationnelle du global
#            1) tous les jours a 00Z:   sortie pour les jours 1 a 5
#                                       depend des fichiers entre 0 et 120h
#                                       sppel: script_pcpn 00 1 5
#            2) tous les jours a 00Z:   sortie pour les jours 6 a 10
#                                       depend des fichiers entre 126 et 240h
#                                       appel: script_pcpn 00 6 10
#            3) tous les dimanches a 00Z:   sortie pour les jours 6 a 10
#                                       depend des fichiers entre 246 et 360h
#                                       appel: script_pcpn 00 11 15
#
#REVISION:   1.1 (original) - 30 janvier 1995
#REVISION:   2.0            - 27 aout 98   - verification AN 2000
#                                            ce script va bien fonctionner
#                                            avec la mise a jour des fonctions RPN
#                                            pour la version Y2K
#REVISION:   2.1            - 22 mars 2006 - removed RUN variable, it is
#					     received via $OCM_RUN	
###############################################################################

#.*****************************************************************************
#.    REVISION 1.2    Conversion a Maestro
#.                    Naysan Saran - 12 Dec. 2013
#.*****************************************************************************  

carteno=${IMG_NAME}
model="GLOBAL"
echel=20.0
MXVA=5
MYVA=5

#---------------------------------------------------------------------
# il est impossible de mettre tous les fichiers dans 1 seul nom
# car on obtient line too long dans le jobout
# on le coupe en jours
#---------------------------------------------------------------------
echo "carte des degres-jours de chauffe entre les jours ${DEBUT} et ${FIN}"
echo "Calcul des precipitations entre les heures ${hdebut} et ${hfin}"

###############################################################################
#  pour calculer les precipitations dans un interval de temps on a besoin
#  des heures de depart et de fin, ainsi que l heure 0
#  nom1=`fgen+ -p prog/glbpres -t ${OCM_RUN} -s 0 -e 0 -c`

nom2=${TASK_INPUT}/nom2
nom3=${TASK_INPUT}/nom3

###############################################################################
cat << EOFPGSM > pgsmdir1
 SORTIE(STD,200,A)
 EXTRAP(VOISIN)
*=======================================
*     grille speciale
*     on couvre plus au sud 
*     et moins au nord
*=======================================
*grille continentale 1:20M  utilise 95km avec le global
*GRILLE(PS, 39, 29, 13.5,  30.0, 190500., 21.0, NORD)
*GRILLE(PS, 153, 113, 51.0, 117.0,  47625., 21.0, NORD)
 GRILLE(PS, 77, 57, 26.0,  70.0,  95250., 21.0, NORD)
 COMPAC=-16
 CHAMP(PCP,${hdebut},${hfin})
EOFPGSM

#
#  execution du pgsm
#
tampon_pgsm=tampon_pgsm
rm -f $tampon_pgsm

${TASK_BIN}/pgsm -iment ${nom2} ${nom3} \
                 -ozsrt $tampon_pgsm \
                 -i pgsmdir1
rm -f pgsmdir1

${TASK_BIN}/voir -iment $tampon_pgsm

###############################################################################
###############################################################################
###############################################################################
cat << EOFSIGMA > sigmadir
#
*
* CARTES SUR VERSATEC
*--------------------
* panneau AYANT LA DIMENSION SUIVANTE
*
*        14.25 X 10.50
*
* ON UTILISE DANS TRAMES HY=2100, HX=1425+1425=2850, ORN=ROT
*
*******************************************************************************
*  SORTIE - FICHIER METACOD ET SEGFILE
*******************************************************************************
*                DIRECTIVES GLOBALES
*******************************************************************************
*
 CLIP = OUI
 NORMID = NON
*
* MICHEL VALIN RECOMMANDE D'AVOIR LISSAGE=NON (le defaut)
* MAIS CE N'EST PAS ACCEPTABLE POUR LES OPERATIONS CAR LES
* CHAMPS SONT TROP BRUYANTS
*
 LISSAGE=OUI
*
*LA FONTE 15 CONTIENT LES MAJUSCULES, LA 16 LES MINUSCULES
*            (ON DOIT UTILISE CW = 2/3 SIZE DANS LE TITRE)
*LA FONTE 10 CONTIENT LES LETTRES MAJUSCULES DES H,L
*LA FONTE 11                      MINUSCULES CORRESPONDANTES
*LA FONTE 17              CHIFFRES DES ETIQUETTES (FONTE ETROITE)
*LES FONTES 2 ET 3 CONTIENNENT LES GRANDS + ET -
*LA FONTE 9 CONTIENT LE CENTRE (X DANS O)
*
*             A  B  C  D  E  F  G  H  I
*   "CASE" 0  1  2  3  4  5  6  7  8  9
 SETFONT([15,16,10,11,12,13, 2, 3,17, 9])
*
* OPTIONS GEOGRAPHIQUES
*
 MAPLAB="ST",1,1,1,1,10,2,NON,OUI,NON,1,.5,OUI
 MAPCOLR=1
 OPTIONS(MAPOPT,["OU","SU","GR",-1.0,"LA","NO"]) 
 OPTIONS(DASHSET,["SMALL",16.0,"NP",1500])
*
* EN UTILISANT 30.0 POUCES
* LES DISTANCES RELATIVES SONT EN 1/100 PO
*
 ECHELLE=$echel
 POUCES=30.0
*
 ECHLGRI=NON
*
* PRECISE UN POINTILLE FIN (TOUJOURS INCLURE "NCRT",1)
*
 OPTIONS(ISPSET,["NCRT",1])
*
************************************************************************........
*                    GROUPE
************************************************************************........
* 
* GROUPE LOT=1-2   POUR LES pcpns
*
*
 GROUPE(1 ,["CO",6,"TH",2,"CA",8,
            "FC",0,"BC",6,"CF","YES",
            "IN",2,"SI",175,"CN",34])
 GROUPE(2 ,["CO",6,"TH",2,"CA",8,
            "FC",0,"BC",6,"CF","YES",
            "IN",2,"SI",175,"CN",34])
*
* GROUPE LOT=3-4-5 POUR LA TAILLE DES etiquettes 
*
 GROUPE(3,["SI",600,"IN",2,"FC",6,"ENH","YES","CN",01]) 
 GROUPE(4,["SI",125,"IN",2,"FC",6,"ENH","YES","CN",01]) 
 GROUPE(5,["SI",225,"IN",2,"FC",6,"ENH","YES","CN",01,"CA",8]) 
*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
*
* ECRITURE ID CARTE
*
* GROUPE 25 POUR TITRE REGIONAL - GLOBAL
* GROUPE 26 POUR CONTENU (DESCRIPTION DES ELEMENTS) + HEURE
*        27      ID CMC (MUMS) et ID petite boite
*        28      CONTENU DESCRIPTIF
*        29      TITRE DE LA CARTE
 GROUPE(25,["CA",0,"SI",225,"IN",2,"FC",1,"CN",01])
 GROUPE(26,["CA",0,"SI",175,"IN",2,"FC",1,"CN",01]) 
 GROUPE(27,["CA",0,"SI",110,"IN",1,"FC",1,"CN",01])
 GROUPE(28,["CA",0,"SI",100,"IN",1,"FC",1,"CN",01])
 GROUPE(29,["CA",0,"SI",175,"IN",2,"FC",1,"CN",01])
*
* SETHPAL NECESSAIRE POUR PRODUIRE EFFACEMENT AVEC PATRON 1
*                         COLORIER LE MOTIF 2 (=099) (VERT - TRANSPARENT)
*                         COLORIER LE MOTIF 3 (=109) (VERT - TRANSPARENT)
*                         COLORIER LE MOTIF 4 (=099) (Rouge  - TRANSPARENT)
*                         COLORIER LE MOTIF 5 (=109) (Rouge  - TRANSPARENT)
*                         COLORIER LE MOTIF 6 (=113) (VERT - TRANSPARENT)
* SETHPAL NECESSAIRE POUR PRODUIRE EFFACEMENT AVEC PATRON 7
*
*               1       2       3       4       5       6       7
 SETHPAL([0001001,0616099,0616109,0216099,0216109,0616113,0001001],7)
*

************************************************************************........
* UTILISE PANNEAU POUR MARQUER LE DEBUT
*
 PANNEAU([0.0, 0.0])
*------------------------------------------------------------------------------ 
*DIRECTIVES POUR LES degres jours
*PRECISE POUR CHAQUE CHAMP LE FACTEUR MULTIPLICATIF ET INTERVALLE CONTOUR
*
 SCAL ("PR", 1000.0, 10.0)
 OPTIONS(ISPSET,["RNDL",1., "RNDV",1.]) 
 OPTIONS(ISPSET,["ECOL",250500750, "LABL",1, "ILAB",1])
 OPTIONS(ISPSET,["MAJR",1, "MINR",1, "NULB", 0)
 OPTIONS(ISPSET,["HCAR","@BH","LCAR","@BL","CCAR","@I9","LHCE",1,"NHL",3])
 OPTIONS(ISPSET,["HILO",3,"CNTR",4,"VALU",5]) 
 OPTIONS(ISPSET,["OFFM",1, "MXVA",$MXVA, "MYVA",$MYVA])
 DASH([1777B])
 OPTIONS(ISPSET,["NCRT",1])
*
 NIVEAUX = 1., 5., 10., 25., 50.,100.,200.
*
 MOTIFS = 000,000,002,003,000,002,003,000
 HAFTON("PR","P" ,-1, ${hdebut},${hfin}, 0, [NIV,PAT,GEO])
 VARIAN("PR","P" ,-1, ${hdebut},${hfin}, 0, [GEO,NIV,PAN])
*******************************************************************************
* BOITE DE LA LEGENDE EN HAUT
* LA CARTE FAIT 14.25po, ON UTILISERA 14.25 po POUR LA LEGENDE
*
*                  1.0           8.12
*                   !             !
*
*                0.0---2.0------------14.25
*                !      !                 !
*     -0.25      !      !                 !                     -0.15 et -0.40
*     -0.65      !      !-----------------!               -0.55
*                !      !                 !                     -0.70
*-0.85           -------!-----------------!               -0.85
*     -0.97 -1.14!      !                 !                     -0.97 et -1.14
*-1.25           -------!-----------------!               -1.25
*
*
*                   !     !         !
*                  1.0   2.20      8.20
*                        2.25      8.25
*                        2.30      8.30
*       texte:           2.50      8.50
*
*
*
*                        2.20    6.20   10.20
*  3 champs:             2.25    6.25   10.25
*                        2.30    6.30   10.30
*       texte:           2.50    6.50   10.50
*******************************************************************************
* ZONE POUR LA LEGENDE (on efface ce qui est deja trace)
*
 BOITE([LEFT,TOP, 0.0,-1.25],[LEFT,TOP,14.25, 0.00],2,1,001)
*
*TIRE UNE LIGNE A .55 et .85
*
 BOITE([LEFT,TOP, 2.00,-0.55],[LEFT,TOP,14.25,-0.55],2,1,000)
 BOITE([LEFT,TOP, 0.00,-0.85],[LEFT,TOP,14.25,-0.85],2,1,000)
*
* LIGNE VERTICALE POUR SEPARER REGIONAL
*
 BOITE([LEFT,TOP, 2.00,-1.25],[LEFT,TOP, 2.00, 0.00],2,1,000)
*
 WRITE([LEFT,TOP, 1.00,-0.25],26, '$model')
# WRITE([LEFT,TOP, 1.00,-0.65],26, 'EXPERIMENTAL')
*
* HEURE DE LA SOURCE
*
 WRITE([LEFT,TOP,  8.12, -0.40],29,
 '@DAT(DATS,H,Z ,DW,-,JS, ,J, ,MON,-,MOI, ,A)',["IN",2])
*
* DESCRIPTION DE LA CARTE
*
 WRITE([LEFT,TOP, 8.12,-0.15],29,
  'FORECAST - PREVISION    DAY-JOUR  ${DEBUT}-${FIN}', ["CNTR",01])
 WRITE([LEFT,TOP, 8.12,-0.70],29,
 'PCPN AMOUNT IN PERIOD  -  HAUTEUR DES PCPN DANS LA PERIODE',
 ["CNTR",01])
*----------------------------------
* EXPLICATION DU CONTENU DES CARTES
* ligne verticale epaisse
*
 LIGNE([LEFT,TOP,10.25,-1.20 ],[LEFT,TOP,10.25,-0.90 ],2,6)
*
 WRITE([LEFT,TOP,10.50,-0.97],28,
 'PCPN',["CNTR",02, "FC",6])
 WRITE([LEFT,TOP,10.50,-1.14],28,
 '(@AM@AM)',["CNTR",02, "FC",6])
*
 BOITE([LEFT,TOP,11.50,-1.20],[LEFT,TOP,11.90,-0.90],1,6,002)
 BOITE([LEFT,TOP,11.90,-1.20],[LEFT,TOP,12.30,-0.90],1,6,003)
 BOITE([LEFT,TOP,12.30,-1.20],[LEFT,TOP,12.70,-0.90],1,6,001)
 BOITE([LEFT,TOP,12.70,-1.20],[LEFT,TOP,13.10,-0.90],1,6,002)
 BOITE([LEFT,TOP,13.10,-1.20],[LEFT,TOP,13.50,-0.90],1,6,003)
 BOITE([LEFT,TOP,13.50,-1.20],[LEFT,TOP,13.90,-0.90],1,6,001)
*
 WRITE([LEFT,TOP,11.70,-1.05],28,'5',["CNTR",01,"ENH","YES", "FC",6])
 WRITE([LEFT,TOP,12.10,-1.05],28,'10',["CNTR",01,"ENH","YES", "FC",6])
 WRITE([LEFT,TOP,12.50,-1.05],28,'25',["CNTR",01,"ENH","YES", "FC",6])
 WRITE([LEFT,TOP,12.90,-1.05],28,'50',["CNTR",01,"ENH","YES", "FC",6])
 WRITE([LEFT,TOP,13.30,-1.05],28,'100',["CNTR",01,"ENH","YES", "FC",6])
 WRITE([LEFT,TOP,13.70,-1.05],28,'200',["CNTR",01,"ENH","YES", "FC",6])
*------------------------------------------------------------------------------
*
* BOITE INFO DU COIN BAS-GAUCHE  (version reduite) (imprime 1 seule fois)
*
* pour utilisation avec le drapeau on utilise une boite plus haute
*
* BOITE([LEFT,BOTTOM,0.05,0.05],[LEFT,BOTTOM,3.0,1.00],2,1,001)
  BOITE([LEFT,BOTTOM,0.05,0.05],[LEFT,BOTTOM,3.0,1.20],2,1,001)
*
* on opte pour CNTR a 12 (au lieu de 01) (coin bas gauche)
* WRITE([LEFT,BOTTOM,  0.15,  0.80],26,
* 'CMC CANADA  @STR(MUMS,A3)',["CNTR",02,"IN",2])
 WRITE([LEFT,BOTTOM,  0.15,  0.50],27,
 '$model',["CNTR",12,"IN",2])
 WRITE([LEFT,BOTTOM,  0.15,  0.30],27,
 '@DAT(DATS,H,Z ,DW,-,JS, ,J, ,MON,-,MOI, ,A)',["CNTR",12,"IN",2])
 WRITE([LEFT,BOTTOM,  0.15,  0.10],27,
 '$JOBNAME',["CNTR",12,"IN",2])
************************************************************************
*
* ON REFAIT LE CADRE PARCE QUE LES INDICATIFS H L PEUVENT L'AVOIR EFFACE
* 
 BOITE([LEFT,BOTTOM, 0.00, 0.0],[RIGHT, BOTTOM, 0.00, 0.00],2,1,000)
 BOITE([LEFT,TOP, 0.00, 0.00],[RIGHT, TOP, 0.00, 0.00],2,1,000)
 BOITE([LEFT,BOTTOM, 0.00, 0.00],[LEFT, TOP, 0.00, 0.00],2,1,000)
 BOITE([RIGHT,BOTTOM, 0.00, 0.00],[RIGHT, TOP, 0.00, 0.000],2,1,000)
************************************************************************........
 SETFONT([15,16, 2, 3, 4, 5, 6, 7, 8, 9])
*
*
* superposition du segment avec le drapeau
*
* segment(nom du segment,
*         coordonnees du point pivot dans le frame,    entier=unite normalise
*                                                      reel = coordonnees utilisateur
*         facteur echelle NUMERATEUR/DENOMINATEUR,     2 entiers
*         rotation en degres,                          entiers
*         point pivot dans l espace segment            entiers, coordonnees pseudo
*
*
* quand on passe a des coordonnees reelles
* on a un resultat dans le coin inferieur gauche
* du panneau superieur droit
* et meme la directive panneau suivante ne corrige pas
* la situation
*       PANNEAU([0.0, 0.0])
*
* On semble donc etre oblige de travailler en coordonnees entieres
*
  SEGMENT ('DRAPO',140,625, 2, 9, 0, 0, 0)
*
 FRAME(0)
EOFSIGMA
#==============================================================================

rm -f segfile
rm -f metacod 

cp ${TASK_INPUT}/segfile segfile

${TASK_BIN}/sigma  -imflds  $tampon_pgsm \
                   -i       sigmadir \
                   -date    ${SEQ_SHORT_DATE}

${TASK_BIN}/trames -device difax \
                   -format rrbx \
                   -ncp 1 \
                   -colors 0 1 1 1 1 1 1 1 \
                   -dn rrbx \
                   -hy 1000 \
                   -size 2660 \
                   -offset 0

${TASK_BIN}/ocxcarte -f ${IMG_NAME}00 -d plot -r ${SEQ_SHORT_DATE}

#
#  charts are sent in gif format to Alberta
#  and Saskatchewan wheater office
#
FTPWAN=${TASK_BIN}/ftpwan

${TASK_BIN}/c_togif rrbx ${ftp_img_name}

${FTPWAN} -D AlWC -f ${ftp_img_name} -d ${AlWC_dest_path}

#
## Saskatoon is closed.
## ${FTPWAN} -D YXE  -f $precip
#
##${FTPWAN} -D PWPIX  -f $precip -d p${today}day$debut-$fin

${FTPWAN} -D PrWC -f ${ftp_img_name} -d ${PrWC_dest_path}
${FTPWAN} -D PWC_2  -f ${ftp_img_name} -d ${PWC_2_dest_path}
### ${FTPWAN} -D OWC -f ${ftp_img_name} -d ${OWC_dest_path}

#
## Kelona is closed.
##  ${FTPWAN} -D YLW -f $precip -d /home/cmc/images/$precip
##  ${FTPWAN} -D PUBFTP -f $precip -d ftp/cmc/dfay/$precip
# the var. PREOP_STATE is defined in suite's .profile_ocm
# operational mode

#scp ${FRONTEND}:$PWD/${ftp_img_name} cmoi@accessdepot:${SCP_dest_path}
c.dissem -c scp_accessdepot -p cmoi -j ${SEQ_NODE} -d /GLSLRO $PWD/${ftp_img_name} 



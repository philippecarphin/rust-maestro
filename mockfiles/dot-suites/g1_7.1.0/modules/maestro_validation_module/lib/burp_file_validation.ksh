#==============================================================================
# Compare two BURP files.
#
# Globals:
#   In  : STATION_TO_DELETE
#   Out : CURRENT_DIFF_STATUS
#   Out : CURRENT_DIFF_MESSAGE
#   Out : NUMBER_OF_FAILED_DIFFS (if the diff failed)
# Arguments:
#   $1 : Path of the fist file.
#   $2 : Path of the second file.
# Returns:
#   None.
#==============================================================================
compare_burp_files ()
{
  file_name=`basename $1`
  diff_option=""

  if [[ ! -z ${STATION_TO_DELETE} ]]
  then
    diff_option="-nostn \>\>${STATION_TO_DELETE}"
  fi

  ret_code=0
  set -ex
  ${TASK_BIN}/burpdiff  $1 $2 ${diff_option} \
                        -diffviewer diff \
                        -oreste \
                        -nodlay \
                        -tmpdir ${TASK_WORK} > ${TASK_OUTPUT}/${file_name}.log 2>&1 || ret_code=$?
  set +ex

  if [ $ret_code -eq 0 ]
  then
    CURRENT_DIFF_STATUS="SUCCESS"
    CURRENT_DIFF_MESSAGE="BURP FILES ARE IDENTICAL"
  else
    #
    # Provide an ascii equivalent of each burp file, with the records
    # sorted.
    #
    burp_file_1_match=`echo -e $1 | sed "s:/:_:g"`_match
    burp_file_2_match=`echo -e $2 | sed "s:/:_:g"`_match
    burp_file_1_reste=`echo -e $1 | sed "s:/:_:g"`_reste
    burp_file_2_reste=`echo -e $2 | sed "s:/:_:g"`_reste

    burp2ascii_1=${TASK_OUTPUT}/`echo -e $1 | sed "s:/:_:g"`_ascii_sorted
    burp2ascii_2=${TASK_OUTPUT}/`echo -e $2 | sed "s:/:_:g"`_ascii_sorted

    liburp ${burp_file_1_match} -delimiteur > ${burp2ascii_1} 2>&1
    liburp ${burp_file_2_match} -delimiteur > ${burp2ascii_2} 2>&1

    liburp ${burp_file_1_reste} -delimiteur >> ${burp2ascii_1} 2>&1
    liburp ${burp_file_2_reste} -delimiteur >> ${burp2ascii_2} 2>&1
    
    CURRENT_DIFF_STATUS="COMPARE FAILED"
    CURRENT_DIFF_MESSAGE="BURP FILES DIFFER"
    NUMBER_OF_FAILED_DIFFS=$((NUMBER_OF_FAILED_DIFFS+1))

    update_diff_advice_burp $1 $2
  fi
}





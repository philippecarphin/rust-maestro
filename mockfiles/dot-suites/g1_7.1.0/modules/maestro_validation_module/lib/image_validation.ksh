#==============================================================================
# Dumps a binary file ($1) in ASCII characters.
# Then deletes byte offsets 1 to 300 (from the dump) and writes the rest in $2.
# From the program rrbxcat, to compare two rrbx files.
#
# Globals:
#   None
# Arguments:
#   $1 : Path of the binary file.
#   $2 : Path of the resulting file.
# Returns:
#   None
#==============================================================================
char_dump ()
{
  od -v -c $1 | sed -e '1,/0000300/d' >> $2
}

#==============================================================================
# Compare two rrbx files (remove the header before).
# Do not allow for small differences.
#
# Globals:
#   Out : CURRENT_DIFF_STATUS
#   Out : CURRENT_DIFF_MESSAGE
#   Out : CURRENT_DIFF_ADVICE (through update_diff_advice_image)
# Arguments:
#   $1 : Path of the first image to compare.
#   $2 : Path of the second image to compare.
# Returns:
#   None
#==============================================================================
compare_rrbx_files_strict ()
{
  char_dump $1 "char_dump_file1"
  char_dump $2 "char_dump_file2"

  ret_code=0
  cmp -s char_dump_file1 char_dump_file2 || ret_code="$?"

  if [[ ${ret_code} -eq 0 ]]
  then
    CURRENT_DIFF_STATUS="SUCCESS"
    CURRENT_DIFF_MESSAGE="RRBX IMAGES ARE IDENTICAL"
  else
    CURRENT_DIFF_STATUS="VALIDATION FAILED"
    CURRENT_DIFF_MESSAGE="RRBX IMAGES DIFFER"
    NUMBER_OF_FAILED_DIFFS=$((NUMBER_OF_FAILED_DIFFS+1))
  fi

  update_diff_advice_image $1 $2
}  

#==============================================================================
# Compare two images with imagemagick's "compare" tool.
# Uses a user defined threshold to allow for small differences.
#
# Globals:
#   In  : COMPARE_THRESHOLD
#   Out : CURRENT_DIFF_STATUS
#   Out : CURRENT_DIFF_MESSAGE
#   Out : CURRENT_DIFF_ADVICE (through update_diff_advice_image)
# Arguments:
#   $1 : Path of the first (imagemagick compliant) image to compare.
#   $2 : Path of the second (imagemagick compliant) image to compare.
# Returns:
#   None
#==============================================================================
compare_with_imagemagick ()
{
  ret_code=0
  cmp -s $1 $2 || ret_code="$?"

  echo "/usr/bin/cmp ret_code (0 : identical, 1 : different) = $ret_code"
  
  if [[ ${ret_code} -eq 0 ]]
  then
    CURRENT_DIFF_STATUS="SUCCESS"
    CURRENT_DIFF_MESSAGE="IMAGES ARE IDENTICAL"
  else
  
      ret_code=0
      err_code=0

      set -ex
      ret_code="$(${TASK_BIN}/compare -metric PSNR  \
                 		      ${1} \
                                      ${2} \
                                      null: 2>&1 || err_code=1 )"
      set +ex

      if [[ ${err_code} -ne 0 && ${ret_code} != *"dissimilar"* ]]
      then
	  CURRENT_DIFF_STATUS="VALIDATION FAILED"
	  CURRENT_DIFF_MESSAGE="IMAGEMAGICK COULD NOT COMPARE THE FILES."
	  NUMBER_OF_FAILED_DIFFS=$((NUMBER_OF_FAILED_DIFFS+1))
	  
	  echo -e "FATAL ERROR : Imagemagik compare did not terminate properly. \
                   Please make sure this task has at least 1G memory."
	  nodelogger  -n ${SEQ_NODE} \
                      -m 'FATAL ERROR : Imagemagik compare did not terminate properly. \
                   Please make sure this task has at least 1G memory.' -s abort
	  exit 1
      fi


      if [[ ${ret_code} == *"dissimilar"* ]]
      then
	  CURRENT_DIFF_STATUS="VALIDATION FAILED"
	  CURRENT_DIFF_MESSAGE="IMAGES DIFFER."
	  NUMBER_OF_FAILED_DIFFS=$((NUMBER_OF_FAILED_DIFFS+1))
	  
      elif [[ ${ret_code} == 'inf' ]]
      then
	  CURRENT_DIFF_STATUS="SUCCESS"
	  CURRENT_DIFF_MESSAGE="IMAGES ARE IDENTICAL."
	  
      elif [[ ${ret_code} > ${COMPARE_THRESHOLD} ]]
      then
	  CURRENT_DIFF_STATUS="SUCCESS"
	  CURRENT_DIFF_MESSAGE="IMAGES ARE SIMILAR."
      else
	  CURRENT_DIFF_STATUS="VALIDATION FAILED"
	  CURRENT_DIFF_MESSAGE="IMAGES DIFFER."
	  NUMBER_OF_FAILED_DIFFS=$((NUMBER_OF_FAILED_DIFFS+1))
      fi
  fi
  update_diff_advice_image $1 $2
}  

#==============================================================================
# Compare two rrbx images with imagemagick's "compare" tool.
# Uses a user defined threshold to allow for small differences.
# Simply converts them to gif files and then calls compare_with_imagemagick().
#
# Globals:
#   Out : CURRENT_DIFF_STATUS
#   Out : CURRENT_DIFF_MESSAGE
# Arguments:
#   $1 : Path of the first (imagemagick compliant) image to compare.
#   $2 : Path of the second (imagemagick compliant) image to compare.
# Returns:
#   None
#==============================================================================
compare_rrbx_files_loose()
{
  img_name=`basename $1`

  ret_code=0
  ${TASK_BIN}/c_togif $1 ${img_name}_set1.gif || ret_code="$?"
  wait

  ret_code=0
  ${TASK_BIN}/c_togif $2 ${img_name}_set2.gif || ret_code="$?"
  wait

  compare_with_imagemagick ${img_name}_set1.gif ${img_name}_set2.gif 
}




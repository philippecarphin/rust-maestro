#==============================================================================
# Compare two data files.
#
# Globals:
#   Out : CURRENT_DIFF_STATUS
#   Out : CURRENT_DIFF_MESSAGE
# Arguments:
#   $1 : Path of the fist file.
#   $2 : Path of the second file.
# Returns:
#   None.
#==============================================================================
compare_data_files ()
{
  ret_code=0
  cmp -s $1 $2 || ret_code="$?"

  if [[ $ret_code -eq 0 ]]
  then
    CURRENT_DIFF_STATUS="SUCCESS"
    CURRENT_DIFF_MESSAGE="FILES ARE IDENTICAL"
  else
    CURRENT_DIFF_STATUS="COMPARE FAILED"
    CURRENT_DIFF_MESSAGE="FILES DIFFER"
    NUMBER_OF_FAILED_DIFFS=$((NUMBER_OF_FAILED_DIFFS+1))
  fi
}  


#==============================================================================
# Compare two text files, removing the first line.
#
# Globals:
#   Out : CURRENT_DIFF_STATUS
#   Out : CURRENT_DIFF_MESSAGE
# Arguments:
#   $1 : Path of the fist file.
#   $2 : Path of the second file.
# Returns:
#   None.
#==============================================================================
compare_bulletin_files ()
{
  tail -n +2 "$1" > file_to_compare1
  tail -n +2 "$2" > file_to_compare2

  #
  # New in V2.2 : the STRINGS_TO_DELETE option
  #
  if [[ ! -z "${STRINGS_TO_DELETE}" ]]
  then
    delete_strings_from_file ${STRINGS_TO_DELETE} file_to_compare1
    delete_strings_from_file ${STRINGS_TO_DELETE} file_to_compare2
  fi

  ret_code=0
  cmp -s file_to_compare1 file_to_compare2 || ret_code="$?"

  if [[ $ret_code -eq 0 ]]
  then
    CURRENT_DIFF_STATUS="SUCCESS"
    CURRENT_DIFF_MESSAGE="BULLETINS ARE IDENTICAL"
  else
    CURRENT_DIFF_STATUS="COMPARE FAILED"
    CURRENT_DIFF_MESSAGE="BULLETINS DIFFER"
    NUMBER_OF_FAILED_DIFFS=$((NUMBER_OF_FAILED_DIFFS+1))
  fi
}

#==============================================================================
# Compare two text files
# Ajout de Michel Van Eeckhout
#==============================================================================
compare_ascii_files ()
{
  cat ${1} > file_to_compare1
  cat ${2} > file_to_compare2

  #
  # New in V2.0 : the STRINGS_TO_DELETE option
  #
  if [[ ! -z "${STRINGS_TO_DELETE}" ]]
  then
    delete_strings_from_file ${STRINGS_TO_DELETE} file_to_compare1
    delete_strings_from_file ${STRINGS_TO_DELETE} file_to_compare2
  fi

  ret_code=0
  cmp -s file_to_compare1 file_to_compare2 || ret_code="$?"

  if [[ $ret_code -eq 0 ]]
  then
    CURRENT_DIFF_STATUS="SUCCESS"
    CURRENT_DIFF_MESSAGE="ASCII FILES ARE IDENTICAL"
  else
    CURRENT_DIFF_STATUS="COMPARE FAILED"
    CURRENT_DIFF_MESSAGE="ASCII FILES DIFFER"
    NUMBER_OF_FAILED_DIFFS=$((NUMBER_OF_FAILED_DIFFS+1))
  fi

}


#==============================================================================
# Compare two cmcarc archives.
# NOTE : The files must be physically inside their directories. This
#        module will not fetch them from a tape.
#
# Globals:
#   Out : CURRENT_DIFF_STATUS
#   Out : CURRENT_DIFF_MESSAGE
# Arguments:
#   $1 : Path of the reference file.
#   $2 : Path of the file to be checked.
# Returns:
#   None.
#==============================================================================
compare_cmcarc_files ()
{
  filename=`basename $1`
  ret_code=0

  reference_dir=${TASK_OUTPUT}/${filename}/reference
  work_dir=${TASK_OUTPUT}/${filename}/work
  logfile=${TASK_OUTPUT}/${filename}/diff_log.txt

  # TODO : This section should go in a config file
  STRINGS_TO_DELETE="fasttmp:pollux:${STRINGS_TO_DELETE}"

  mkdir -p ${reference_dir}
  mkdir -p ${work_dir}

  cp $1 ${reference_dir}
  cp $2 ${work_dir}

  for directory in ${reference_dir} ${work_dir}
  do
    cd ${directory}
    cmcarc -xv -f ${filename} || ret_code="$?"
    rm ${filename}

    for f in `ls *`
    do
      delete_strings_from_file ${STRINGS_TO_DELETE} ${f}
    done
  done


  set -x
  diff -r ${TASK_OUTPUT}/${filename}/work ${TASK_OUTPUT}/${filename}/reference > ${logfile} 2>&1 || ret_code="$?"
  set +x

  if [[ $ret_code -eq 0 ]]
  then
    CURRENT_DIFF_STATUS="SUCCESS"
    CURRENT_DIFF_MESSAGE="CMCARC FILES ARE IDENTICAL"
  else
    CURRENT_DIFF_STATUS="COMPARE FAILED"
    CURRENT_DIFF_MESSAGE="CMCARC FILES DIFFER"
    NUMBER_OF_FAILED_DIFFS=$((NUMBER_OF_FAILED_DIFFS+1))

    update_diff_advice_cmcarc $1 $2
  fi

  cd ${TASK_WORK}/

}





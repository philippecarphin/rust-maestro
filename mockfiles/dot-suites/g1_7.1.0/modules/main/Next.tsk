#!/bin/ksh
#.*****************************************************************************
#.
#.     JOB NAME - Next
#.
#.     STATUS - OPERATIONAL - ESSENTIAL
#.
#.     DESCRIPTION - This task gets a new date from the list and launch the '/main'
#.                   module at that new date.  
#.
#.*****************************************************************************

# Retrieve arguments if provided
passed=${1}

if [ "${MAIN_next_mode}" = datelist ]; then
    # Obtain a lock on the date list
    local_remaining=${TASK_WORK}/my_remaining.dat
    timeout=60
    count=0
    mv ${date_repo}/${date_file} ${local_remaining} || true
    while [[ ! -f "${local_remaining}" && "${count}" -lt "${timeout}" ]] ; do
	sleep 1
	mv ${date_repo}/${date_file} ${local_remaining} || true
	count=$((count+1))
    done
    if [[ "${count}" -ge "${timeout}" ]] ; then
	${SEQ_BIN}/nodelogger -n ${SEQ_NODE} -s infox -m "Error: timeout waiting to obtain lock in ${SEQ_NODE}"
	exit 1
    fi

#filter out empty lines
    grep -v '^ *$' ${local_remaining} > ${local_remaining}.tmp || true
    mv ${local_remaining}.tmp ${local_remaining}

# Check for an empty list of dates
    have_next_date=1
    if [[ "$(cat ${local_remaining} | wc -l)" -lt 1 ]] ; then
	${SEQ_BIN}/nodelogger -n ${SEQ_NODE} -s infox -m "Cycle complete:  no more dates to process"
	have_next_date=0
	mv ${local_remaining} ${date_repo}/${date_file}
    fi

# Pop next date from the list
    if [[ "${have_next_date}" -gt 0 ]] ; then
	list=${TASK_WORK}/list
	cp ${local_remaining} ${list}
	newdate=$(head -n 1 ${list})
	tail -n +2 ${list} >${date_repo}/${date_file}
    fi
elif [ "${MAIN_next_mode}" = live ]; then
    have_next_date=1
    newdate=$(r.date -V ${SEQ_DATE} +${MAIN_next_interval} | cut -c-14)
else
    echo "This mode MAIN_next_mode=${MAIN_next_mode} is not supported.  Please choose 'datelist' or 'live'."
    exit 1
fi

# Launch next date
if [[ "${have_next_date}" -gt 0 ]] ; then
    ${SEQ_BIN}/nodelogger -n ${SEQ_NODE} -s infox -m "Submitting date ${newdate} with 'expbegin'"
    ${TASK_BIN}/expbegin -d ${newdate}
fi

# End the current experiment if in launch mode
if [[ "$(echo ${passed} | grep boot_threads | wc -w)" -gt 0 ]] ; then
    nthreads=$(echo ${passed} | cut -d '=' -f 2)
    if [[ "${nthreads}" -gt 0 ]] ; then
	${SEQ_BIN}/nodelogger -n ${SEQ_NODE} -s infox -m "Submitting ${SEQ_NODE} thread=${nthreads}"
	maestro -n ${SEQ_NODE} -s submit -o "-args boot_threads=$((${nthreads}-1))" -i
    fi
fi

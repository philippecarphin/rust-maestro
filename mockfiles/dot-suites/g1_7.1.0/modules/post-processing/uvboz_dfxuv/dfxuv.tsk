#! /bin/ksh
###############################################################################
#CMOI_LEVEL oprun
#CMOI_PLATFORM op_f
#
#
# Alain Bergeron 1998/07/27
# Utilisation des nouvelles options de dtstmp pour conformite a l'an 2000
#
# Marc Klasa 2000/05/23
# Ajouter les 'H' et 'L' sur la carte de l'ozone
#
# Marc Klasa 2004/02/18
# Nouvel Indice UV corrige pour neige, altitude et reponse angulaire de
# Brewer (lorsque ensoleille) 
# "EE" change pour "EF"
#
###############################################################################


echo ${HEURE}

#-----------------------------------------------------------------------------
#  pgsm va chercher les champs dans les projections desirees
#  les cartes de controles pour pgsm
#
#
#  execution du pgsm
#
cat << EOFPGSM > pgsmdir
 SORTIE(STD,30,A)
 EXTRAP(VOISIN)
*
* GRILLE(PS,191,141, 101.0, 101.0,  76200., 21.0, NORD)
* GRILLE(PS, 77, 57,  41.0,  41.0, 190500., 21.0, NORD)
  GRILLE(PS, 39, 29,  21.0,  21.0, 381000., 21.0, NORD)
*  GRILLE(PS, 20, 15,  11.0,  11.0, 762000., 21.0, NORD)
 COMPAC=-16 
 IP3ENT=${JUL}
 HEURE(TOUT)
 CHAMP('DU')
 CHAMP('EF')
EOFPGSM

rm -f femap

${TASK_BIN}/pgsm -iment ${TASK_INPUT}/uvo3fld -ozsrt femap -i pgsmdir 

cat << EOFSIGMA > sigmadir
###############################################################################
#
# 
* DIRECTIVES SIGMA POUR PRODUIRE OZONE
* carte 2 panneaux contenant en haut le champ DU et en bas le champ EF
* ces champs sont produits par les programmes venant de Downsview
* et adaptes par Pierre KOCLAS
*******************************************************************************
* 22 avril 1992
* GILLES DESAUTELS (DDO)
*******************************************************************************
* CARTES SUR VERSATEC
*--------------------
* ON TRACE ICI 2 PANNEAUX COTE A COTE EN UTILISANT L'ECHELLE 1:40M
* CHACUN AYANT LA DIMENSION SUIVANTE 
* ICI ON UTILISE RESOLUTION DE 190.5KM
*
*        14.25 X 10.50
*
*
* COMME LA SECONDE CARTE COMMENCE A 10.50 PO SELON Y
*                                   14.25          X
* dans trames on fait comme si on avait 4 panneaux
* ON UTILISE DANS TRAMES HY=2100, HX=1425+1425=2850, ORN=ROT
*******************************************************************************
*
*                DIRECTIVES SIGMA
*
*                DIRECTIVES GLOBALES
*
*******************************************************************************
 CLIP = OUI
 NORMID = NON
 LISSAGE=OUI
*
*LA FONTE 15 CONTIENT LES MAJUSCULES, LA 16 LES MINUSCULES
*LA FONTE 10 CONTIENT LES LETTRES MAJUSCULES DES H,L
*LA FONTE 11 CONTIENT LES LETTRES MINUSCULES CORRESPONDANTES
*LA FONTE 17 CONTIENT LES CHIFFRES DES ETIQUETTES (FONTE ETROITE)
*LA FONTE  2 CONTIENT LES GRANDS + ET -
*LA FONTE  3 CONTIENT LES GRANDS LETTRES GRECQUES
*LA FONTE  9 CONTIENT LE CENTRE (X DANS O)
*
*   @         A  B  C  D  E  F  G  H  I
*   "CASE" 0  1  2  3  4  5  6  7  8  9
 SETFONT([15,16,10,11,12,13, 2, 3,17, 9])
*
* OPTIONS GEOGRAPHIQUES
*
 MAPLAB="ST",1,1,1,1,10,2,NON,OUI,NON,1,.5,OUI
 MAPCOLR=1
 OPTIONS(MAPOPT,["OU","GL","GR",-1.0,"LA","NO"]) 
 OPTIONS(DASHSET,["SMALL",16.0,"NP",1500])
*
 ECHELLE=40.0 
 POUCES=30.0
*
 ECHLGRI=NON
*
 OPTIONS(ISPSET,["NCRT",2])
*
* SETHPAL NECESSAIRE POUR PRODUIRE EFFACEMENT AVEC PATRON 1
*                         COLORIER LE MOTIF 2 (=099) (VERT - TRANSPARENT)
*                         COLORIER LE MOTIF 3 (=109) (VERT - TRANSPARENT)
*                         COLORIER LE MOTIF 4 (=099) (VIOLET-TRANSPARENT)
*                         COLORIER LE MOTIF 5 (=109) (VIOLET-TRANSPARENT)
*                         COLORIER LE MOTIF 6 (=113) (VERT - TRANSPARENT)
*
*               1       2       3       4       5       6
 SETHPAL([0001001,0616099,0616109,0516099,0516109,0616113],6)
*
************************************************************************........
*                    GROUPE
************************************************************************........
* 
* GROUPE LOT=6-7   POUR LES ISOHYPSES DES CHAMPS <DU> et <EF> 6-majeur (TH=2)
*                  COULEUR CO=7 (BLEU)                        7-mineur (TH=2)
*                  BOITE FOND NOIR
*
 GROUPE(6,["CO",7,"TH",2,"CA",8,
           "FC",0,"BC",7,"CF","YES",
           "IN",1,"SI",175,"CN",34])
 GROUPE(7,["CO",7,"TH",2,"CA",8,
           "FC",0,"BC",7,"CF","YES",
           "IN",1,"SI",175,"CN",34])
*
* GROUPE LOT=8-9-10 POUR LA TAILLE DES MAX-MIN DES <DU>      (H L)
* a noter in=3 pour avoir un H L completement plein
*
 GROUPE( 8,["SI",600,"IN",3,"FC",7,"ENH","YES","CN",01]) 
 GROUPE( 9,["SI",125,"IN",2,"FC",7,"ENH","YES","CN",01]) 
 GROUPE(10,["SI",225,"IN",2,"FC",7,"ENH","YES","CN",01,"CA",8]) 
*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
*
* ECRITURE ID CARTE
*
* GROUPE 25 POUR TITRE PLAN VERT
* GROUPE 26 POUR ID DU CMC ET DESCRIPTION DU CONTENU (BOITE NORMALE)
*        27      HEURE DE VALIDITE et ecriture dans les zones ombragees
*        28      CONTENU DESCRIPTIF et unites
*        29      TITRE DE LA CARTE
*
* "CA",0 => FONTE 15  (defaut)
* "CA",8 => FONTE  8  (etroit)
*
 GROUPE(25,["CA",0,"SI",225,"IN",2,"FC",1,"CN",01])
 GROUPE(26,["CA",0,"SI",100,"IN",1,"FC",1,"CN",01])
 GROUPE(27,["CA",0,"SI",100,"IN",1,"FC",1,"CN",01])
 GROUPE(28,["CA",0,"SI",100,"IN",1,"FC",1,"CN",01])
 GROUPE(29,["CA",0,"SI",160,"IN",2,"FC",1,"CN",01])
*
*------------------------------------------------------------------------------
* ID DU CMC ET HEURE DE VALIDITE A GAUCHE
*
 MUMS(["204","204","202","202"])
*
*------------------------------------------------------------------------------
*
************************************************************************
******************************************************* BAS-GAUCHE *****
******************************************************* champ EF   *****
************************************************************************
 PANNEAU([0.0, 0.0]) 
*
* OFFM FAIT QUE LE MESSAGE EN BAS DU GRAPHIQUE EST ELIMINE
* MXVA ET MYVA PERMET D'ELIMINER LES MAX-MIN TROP RAPPROCHES
* ON PEUT FAIRE VARIER CES NOMBRES SELON LE BRUIT DU CHAMP
*                                        et la distance de grille
*
* POUR UNE CARTE 1:40M distance = 3/16  po 
*      donc la distance minimum entre 2 H ou 2 L sera de
*       4 * 3/16 po = 0.75 po  si MXVA,MYVA=2
*
* OPTIONS(ISPSET,["OFFM",1, "MXVA",2, "MYVA",2])
************************************************************************........
*
 SCAL ("EF",  .04, 1.0)
 LIMITE=  0., +200.
 DASH([1777B])
*
* NIVEAUX = -2.5, +2.5
* MOTIFS =000,004,000
* HAFTON("EE","F",-1, 0, 0, 0,[GEO,NIV,PAT])
*
*
 OPTIONS(ISPSET,["RNDL",1., "RNDV",1.])
 OPTIONS(ISPSET,["ECOL",350650950, "LABL", 6, "ILAB",1])
 OPTIONS(ISPSET,["MAJR", 6, "MINR", 7, "NULB",99])
*
*DIRECTIVES POUR LES MAX-MIN DES <EF>
*
* OPTIONS(ISPSET,["HCAR","@BH","LCAR","@BL","CCAR","@I9","LHCE",1,"NHL",3])
 OPTIONS(ISPSET,["HCAR"," ","LCAR"," ","CCAR"," ","LHCE",1,"NHL",3])
 OPTIONS(ISPSET,["HILO", 8,"CNTR", 9,"VALU",10])
 OPTIONS(ISPSET,["OFFM",1, "MXVA",2, "MYVA",2])
*
# VARIAN("EE","P",-1,12000, ${HEURE}, 0,[GEO,PAN])
 VARIAN("EF","P",-1,12000, ${HEURE},${JUL},[GEO,PAN])
*
*******************************************************************************
* BOITE DE LA LEGENDE EN HAUT
* LA CARTE FAIT 14.25po, ON UTILISERA 14.25 po POUR LA LEGENDE
*
*                  1.0           8.12
*                   !             !
*
*                0.0---2.0------------14.25
*     -0.18      !GREEN !    LABEL        !             
*     -0.42      ! PLAN !    EN-TETE      !                     -0.20 et -0.40
*     -0.60      !------!-----------------!               -0.60
*           -0.78!PLAN  !      heure      !                     -0.72
*-0.85      -1.02-VERT  !-----------------!               -0.85
*     -0.97 -1.14!      !     details     !                     -0.97 et -1.14
*-1.25           -------!-----------------!               -1.25
*
*
*                   !     !         !
*                  1.0   2.20      8.20
*                        2.25      8.25
*                        2.30      8.30
*       texte:           2.50      8.50
*
*
*
*                        2.20    6.20   10.20
*  3 champs:             2.25    6.25   10.25
*                        2.30    6.30   10.30
*       texte:           2.50    6.50   10.50
*******************************************************************************
* ZONE POUR LA LEGENDE (on efface ce qui est deja trace)
*                      (devra se faire apres le contourage
*                       lorsque la fenetre du global
*                       couvrira la zone entiere puisqu'on
*                       doit effacer ce qui est deja la)
*
 BOITE([LEFT,TOP, 0.0,-1.25],[LEFT,TOP,14.25, 0.00],2,1,001)
*
*TIRE UNE LIGNE A .60 et .85
*
 LIGNE([LEFT,TOP, 0.00,-0.60],[LEFT,TOP,14.25,-0.60],2,1)
 LIGNE([LEFT,TOP, 2.00,-0.85],[LEFT,TOP,14.25,-0.85],2,1)
*
* LIGNE VERTICALE POUR SEPARER PLAN VERT
*
 LIGNE([LEFT,TOP, 2.00,-1.25],[LEFT,TOP, 2.00, 0.00],2,1)
*
* WRITE([LEFT,TOP, 1.00,-0.18],25, 'GREEN')
* WRITE([LEFT,TOP, 1.00,-0.42],25, 'PLAN')
 WRITE([LEFT,TOP, 1.00,-0.30],25, 'UVB')
* WRITE([LEFT,TOP, 1.00,-0.78],25, 'PLAN')
* WRITE([LEFT,TOP, 1.00,-1.02],25, 'VERT')
*
* HEURE DE VALIDITE
*
 WRITE([LEFT,TOP,  8.12, -0.72],27,
 'P${HEURE}H V@DAT(DATV,H,Z  ,DW,-,JS, ,J, ,MON,-,MOI, ,A)')
*
* DESCRIPTION DE LA CARTE
*
 WRITE([LEFT,TOP, 8.12,-0.20],29,
 'ULTRAVIOLET INDEX', ["CNTR",01])
* 'NOON GROUND LEVEL UV-@AB ESTIMATE', ["CNTR",01])
 WRITE([LEFT,TOP, 8.12,-0.40],29,
 'INDICE D ULTRAVIOLET', ["CNTR",01])
* 'ESTIME UV-@AB ATTEIGNANT LE SOL A MIDI', ["CNTR",01])
*
* EXPLICATION DU CONTENU DES CARTES
* champ EF
*
* ligne verticale
*
 LIGNE([LEFT,TOP, 2.25,-1.20 ],[LEFT,TOP,2.25,-0.90 ],2,7)
*
 WRITE([LEFT,TOP, 2.50,-0.97 ],28, 'UV INDEX - INDICE UV',
 ["CNTR",02, "FC",7])
 WRITE([LEFT,TOP, 2.50,-1.14],28,
 '0, 1, 2...10',["CNTR",02, "FC",7])
* '@I9 @BH @BL   0, 1, 2...10',["CNTR",02, "FC",7])
 WRITE([LEFT,TOP, 8.00,-0.97 ],28,
 'CLEAR SKY LOCAL SOLAR NOON VALUES', ["CNTR",02, "FC",7])
 WRITE([LEFT,TOP, 8.00,-1.14 ],28,
 'VALEURS AU MIDI SOLAIRE LOCAL SOUS UN CIEL DEGAGE', ["CNTR",02, "FC",7])
*
*------------------------------------------------------------------------------
* BOITE INFO DU COIN BAS-GAUCHE  (version reduite) (imprime 1 seule fois)
*
 BOITE([LEFT,BOTTOM,0.05,0.05],[LEFT,BOTTOM,3.0,1.20],2,1,001)
*
 WRITE([LEFT,BOTTOM,  0.15,  0.50],26,
 'OZONE - UVB',["CNTR",12,"IN",2])
 WRITE([LEFT,BOTTOM,  0.15,  0.30],26,
 '@DAT(DATS,H,Z  ,DW,-,JS, ,J, ,MON,-,MOI, ,A)',["CNTR",12,"IN",2])
 WRITE([LEFT,BOTTOM,  0.15,  0.10],26,
 ' $JOBNAME          @STR(MUMS,A3)',["CNTR",12,"IN",2])
*
************************************************************************
*
* ON REFAIT LE CADRE PARCE QUE LES INDICATIFS H L PEUVENT L'AVOIR EFFACE
*
 LIGNE([LEFT,BOTTOM, 0.00, 0.0],[RIGHT, BOTTOM, 0.00, 0.00],2,1)
 LIGNE([LEFT,TOP, 0.00, 0.00],[RIGHT, TOP, 0.00, 0.00],2,1)
 LIGNE([LEFT,BOTTOM, 0.00, 0.00],[LEFT, TOP, 0.00, 0.00],2,1)
 LIGNE([RIGHT,BOTTOM, 0.00, 0.00],[RIGHT, TOP, 0.00, 0.000],2,1)
*
************************************************************************
******************************************************* HAUT-GAUCHE*****
******************************************************* champ DU   *****
************************************************************************
 PANNEAU([ 0.00,10.5]) 
*
 SCAL ("DU",   1.0,10.0)
 LIMITE=100., 600.
 DASH([1777B])
*
 OPTIONS(ISPSET,["RNDL",1., "RNDV",1.])
 OPTIONS(ISPSET,["ECOL",350650950, "LABL", 6, "ILAB",1])
 OPTIONS(ISPSET,["MAJR", 6, "MINR", 7, "NULB",99])
*
*DIRECTIVES POUR LES MAX-MIN DES <DU>
*
# Ajouter 'H' et 'L' - Marc Klasa - mai 2000 
*
 OPTIONS(ISPSET,["HCAR","@BH","LCAR","@BL","CCAR","@I9","LHCE",1,"NHL",3])
 OPTIONS(ISPSET,["HILO", 8,"CNTR", 9,"VALU",10])
 OPTIONS(ISPSET,["OFFM",1, "MXVA",2, "MYVA",2])
*
# VARIAN("DU","P",-1,12000, ${HEURE}, 0,[GEO,PAN])
 VARIAN("DU","P",-1,12000, ${HEURE}, ${JUL},[GEO,PAN])
************************************************************************
* ZONE POUR LA LEGENDE
*
 BOITE([LEFT,TOP, 0.0,-1.25],[LEFT,TOP,14.25, 0.00],2,1,001)
*
*TIRE UNE LIGNE A .60 et .85
*
 LIGNE([LEFT,TOP, 0.00,-0.60],[LEFT,TOP,14.25,-0.60],2,1)
 LIGNE([LEFT,TOP, 2.00,-0.85],[LEFT,TOP,14.25,-0.85],2,1)
*
* LIGNE VERTICALE POUR SEPARER PLAN VERT
*
 LIGNE([LEFT,TOP, 2.00,-1.25],[LEFT,TOP, 2.00, 0.00],2,1)
*
* WRITE([LEFT,TOP, 1.00,-0.18],25, 'GREEN')
* WRITE([LEFT,TOP, 1.00,-0.42],25, 'PLAN')
* WRITE([LEFT,TOP, 1.00,-0.78],25, 'PLAN')
* WRITE([LEFT,TOP, 1.00,-1.02],25, 'VERT')
 WRITE([LEFT,TOP, 1.00,-0.30],25, 'OZONE')
*
* HEURE DE VALIDITE
*
 WRITE([LEFT,TOP,  8.12, -0.72],27,
 'P${HEURE}H V@DAT(DATV,H,Z  ,DW,-,JS, ,J, ,MON,-,MOI, ,A)')
*
* DESCRIPTION DE LA CARTE
*
 WRITE([LEFT,TOP, 8.12,-0.20],29,
 'TOTAL OZONE', ["CNTR",01])
 WRITE([LEFT,TOP, 8.12,-0.40],29,
 'OZONE TOTAL', ["CNTR",01])
*
* EXPLICATION DU CONTENU DES CARTES
* champ DU
*
* ligne verticale
*
 LIGNE([LEFT,TOP, 2.25,-1.20 ],[LEFT,TOP,2.25,-0.90 ],2,7)
*
 WRITE([LEFT,TOP, 2.50,-0.97 ],28, 'DOBSON UNIT - UNITE DOBSON',
 ["CNTR",02, "FC",7])
 WRITE([LEFT,TOP, 2.50,-1.14],28,
* '@I9 @BH @BL   ...300, 310, 320...',["CNTR",02, "FC",7])
 '...300, 310, 320...',["CNTR",02, "FC",7])
*
*------------------------------------------------------------------------------
*
* ON REFAIT LE CADRE PARCE QUE LES INDICATIFS H L PEUVENT L'AVOIR EFFACE
*
 LIGNE([LEFT,BOTTOM, 0.00, 0.0],[RIGHT, BOTTOM, 0.00, 0.00],2,1)
 LIGNE([LEFT,TOP, 0.00, 0.00],[RIGHT, TOP, 0.00, 0.00],2,1)
 LIGNE([LEFT,BOTTOM, 0.00, 0.00],[LEFT, TOP, 0.00, 0.00],2,1)
 LIGNE([RIGHT,BOTTOM, 0.00, 0.00],[RIGHT, TOP, 0.00, 0.000],2,1)
*
**********************************************************************
* il faut utiliser la meme description de fontes que celle du
* programme fortran drapeau.f qui a genere le segment 'DRAPO'
* a savoir: DATA NEWFONT/15,16,2,3,4,5,6,7,8,9/
*
 SETFONT([15,16, 2, 3, 4, 5, 6, 7, 8, 9])
*
*
* superposition du segment avec le drapeau
*
* segment(nom du segment,
*         coordonnees du point pivot dans le frame,    entier=unite normalise
*                                                      reel = coordonnees utilisateur
*         facteur echelle NUMERATEUR/DENOMINATEUR,     2 entiers
*         rotation en degres,                          entiers
*         point pivot dans l espace segment            entiers, coordonnees pseudo
*
* cet appel donne de tres grosses lettres
*  SEGMENT ('DRAPO', 1, 1, 1, 1, 0, 0, 0)
* cet appel est de bonne dimension mais mal place
*  SEGMENT ('DRAPO', 1, 1, 1, 4, 0, 0, 0)
*
* quand on passe a des coordonnees reelles
* on a un resultat dans le coin inferieur gauche
* du panneau superieur droit
* et meme la directive panneau suivante ne corrige pas
* la situation
*       PANNEAU([0.0, 0.0])
*
* On semble donc etre oblige de travailler en coordonnees entieres
*
  SEGMENT ('DRAPO',140,625, 2, 9, 0, 0, 0)
*
**********************************************************************
 FRAME(0) 
*
EOFSIGMA

rm -f metacod segfile

cp ${TASK_INPUT}/segfile segfile

${TASK_BIN}/sigma  -imflds femap -i sigmadir -date $RUN 

${TASK_BIN}/trames  -device difax \
                    -format rrbx \
                    -ncp 1 -colors 0 1 1 1 1 1 1 1 \
                    -dn rrbx \
                    -offset 0 -size 3000 -hx 1425 -hy 2100

${TASK_BIN}/ocxcarte -t -f ${IMG_NAME} -d difax -r ${SEQ_SHORT_DATE} -w





# Commenter les dependances sur CMDW_FULL_G1_EXP
cd main;
git grep -l CMDW_FULL_G1_EXP | xargs sed -i 's%<DEPENDS.*CMDW_FULL_G1_EXP.*/>%<!--&-->%g'

# Commenter les dependances sur CMDW_FULL_G2_EXP
git grep -l CMDW_FULL_G2_EXP | xargs sed -i 's%<DEPENDS.*CMDW_FULL_G2_EXP.*/>%<!--&-->%g'

# Commenter les dependances sur CMDW_POST_PROC_G6_EXP
git grep -l CMDW_POST_PROC_G6_EXP | xargs sed -i 's%<DEPENDS.*CMDW_POST_PROC_G6_EXP.*/>%<!--&-->%g'

# Commenter les dependances sur CMDW_TRANSER_R1_EXP
git grep -l CMDW_TRANSFER_R1_EXP | xargs sed -i 's%<DEPENDS.*CMDW_TRANSFER_R1_EXP.*/>%<!--&-->%g'

# Commenter les dependances sur CMDW_POST_PROC_R1_EXP
git grep -l CMDW_POST_PROC_R1_EXP | xargs sed -i 's%<DEPENDS.*CMDW_POST_PROC_R1_EXP.*/>%<!--&-->%g'

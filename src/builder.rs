use std::cell::RefCell;
use std::error::Error;
use std::rc::{Rc, Weak};

use crate::node::{DepData, FlowType, Node, NodeRef};

type NodeBuilderRef = Rc<RefCell<NodeBuilder>>;
type NodeBuilderWeakRef = Weak<RefCell<NodeBuilder>>;

/// The NodeBuilder struct holds a real node and temporary
/// information required for the building of the complete
/// node graph with container-containee realationship and
/// the submitter-submittee relationship.  When a SUBMITS
/// node is encountered, the node it refers to has not
/// necessarily been created so we cannot create a reference
/// to it.  Instead we create a tree of NodeBuilders and
/// once all nodes are created, we traverse the graph of
/// NodeBuilders to complete the child, parent, submits,
/// and submitter references in the graph of real nodes.
/// Finally the root node builder gives up its real_node,
/// goes out of scope, all the NodeBuilders go out of scope
/// leaving a completed graph of nodes.
#[derive(Debug)]
pub struct NodeBuilder {
    pub real_node: NodeRef,
    pub children: Vec<NodeBuilderRef>,
    pub parent: NodeBuilderWeakRef,
    pub sub_names: Vec<String>,
}

impl NodeBuilder {
    pub fn from_xml_source(
        xml_source: Rc<elementtree::Element>,
        parent: Option<&NodeBuilderRef>,
    ) -> Result<Self, Box<dyn Error>> {
        let tag = xml_source.tag().to_string();
        let name = xml_source
            .get_attr("name")
            .ok_or(format!(
                "Element {:#?} does not have 'name' attribute",
                xml_source
            ))?
            .to_string();
        let flow_type = FlowType::from_string(&tag)
            .ok_or(format!("Cannot create NodeBuilder from {tag} XML node"))?;
        let b = Self {
            real_node: Rc::new(RefCell::new(Node::new(name, flow_type))),
            children: vec![],
            parent: match parent {
                Some(p) => Rc::downgrade(p),
                None => Weak::new(),
            },
            sub_names: vec![],
        };

        Ok(b)
    }
    fn find_submits_node(&self, sub_name: &str) -> Result<NodeRef, Box<dyn Error>> {
        let node_to_search = match self.real_node.borrow().flow_type {
            FlowType::Task | FlowType::NPassTask => self
                .parent
                .upgrade()
                .ok_or("Task with no parent")?
                .borrow()
                .real_node
                .clone(),
            _ => self.real_node.clone(),
        };
        for n in &node_to_search.borrow().children {
            if n.borrow().name == sub_name {
                return Ok(n.clone());
            }
        }
        Err(format!(
            "Submits node with name '{}' not found for submitter node '{}'",
            sub_name,
            self.real_node.borrow().name
        )
        .into())
    }
    fn assign_real_node_children(&self) -> Result<(), Box<dyn Error>> {
        for bc in &self.children {
            self.real_node
                .borrow_mut()
                .children
                .push(bc.borrow().real_node.clone());
            bc.borrow().assign_real_node_children()?;
        }
        Ok(())
    }
    fn assign_real_node_parents(&self) -> Result<(), Box<dyn Error>> {
        for bc in &self.children {
            bc.borrow().real_node.borrow_mut().parent = Rc::downgrade(&self.real_node);
            bc.borrow().assign_real_node_parents()?;
        }
        Ok(())
    }
    fn set_exp_home(&self, exp_home: &str) -> Result<(), Box<dyn Error>> {
        self.real_node.borrow_mut().exp_home = exp_home.to_string();
        for bc in &self.children {
            bc.borrow().set_exp_home(exp_home)?
        }
        Ok(())
    }
    fn resolve_submits(&self) -> Result<(), Box<dyn Error>> {
        for sub_name in &self.sub_names {
            let sub_node = self.find_submits_node(sub_name)?;
            self.real_node
                .borrow_mut()
                .submits
                .push(Rc::downgrade(&sub_node));
            sub_node.borrow_mut().submitter = Rc::downgrade(&self.real_node);
        }
        let flow_type = self.real_node.borrow().flow_type;
        match flow_type {
            FlowType::Switch => {
                for switch_item in &self.children {
                    let mut real_node = self.real_node.borrow_mut();
                    let real_switch_item = &switch_item.borrow().real_node;
                    real_node.submits.push(Rc::downgrade(real_switch_item));
                    real_switch_item.borrow_mut().submitter = Rc::downgrade(&self.real_node);
                }
            }
            _ => {}
        };
        for c in &self.children {
            c.borrow().resolve_submits()?;
        }
        Ok(())
    }
    pub fn build(&self, exp_home: &str) -> Result<NodeRef, Box<dyn Error>> {
        self.assign_real_node_children()?;
        self.assign_real_node_parents()?;
        self.resolve_submits()?;
        self.set_exp_home(exp_home)?;
        Ok(self.real_node.clone())
    }
}

struct ExperimentBuilder {
    exp_home: std::path::PathBuf,
}

impl ExperimentBuilder {
    // TODO: Do I really need ExperimentBuilder?  Could &self not just be
    //       a std::path::PathBuf for the experiment home?
    fn do_xml_node(
        &self,
        flow_node_builder: &NodeBuilderRef,
        xml_node: &elementtree::Element,
    ) -> Result<(), Box<dyn Error>> {
        for xc in xml_node.children() {
            let tag = xc.tag().to_string();
            match tag.as_str() {
                "SUBMITS" => {
                    let sub_name = xc
                        .get_attr("sub_name")
                        .ok_or("SUBMITS node with no attribute 'sub_name'")?;
                    flow_node_builder
                        .borrow_mut()
                        .sub_names
                        .push(sub_name.to_string());
                }
                "DEPENDS_ON" => {
                    let dep = DepData::from_xml_source(&xc)?;
                    flow_node_builder
                        .borrow()
                        .real_node
                        .borrow_mut()
                        .depends_on
                        .push(dep);
                }
                "MODULE" => {
                    let module_name = xc.get_attr("name").ok_or("MODULE node with no name")?;
                    let module_filename = self
                        .exp_home
                        .join("modules")
                        .join(module_name)
                        .join("flow.xml");
                    let child = self.parse_xml(&module_filename, Some(&flow_node_builder))?;
                    /*
                     * IMPORTANT: We have *this* MODULE node whose name refers
                     * represents a reference to a module in another XML file
                     * The root MODULE node in *that* module file may be
                     * different.  We need the node that we create here to have
                     * the name of *this* module node.  So after doing parse_xml,
                     * (which sets the name according to the *that* module's
                     * flow.xml, we set the name back to *this* module's name
                     *
                     * Otherwise, submits searches would fail.
                     */
                    child.borrow().real_node.borrow_mut().name = module_name.to_string();
                    flow_node_builder.borrow_mut().children.push(child);
                }
                _ => {
                    let child = Rc::new(RefCell::new(NodeBuilder::from_xml_source(
                        xc.clone().into(),
                        Some(&flow_node_builder),
                    )?));
                    self.do_xml_node(&child, xc)?;
                    flow_node_builder.borrow_mut().children.push(child);
                }
            };
        }
        Ok(())
    }

    fn parse_xml(
        &self,
        filename: &std::path::PathBuf,
        parent: Option<&NodeBuilderRef>,
    ) -> Result<NodeBuilderRef, Box<dyn Error>> {
        let f = std::fs::File::open(filename)?;
        let root = Rc::new(elementtree::Element::from_reader(f)?);
        let root_builder = Rc::new(RefCell::new(NodeBuilder::from_xml_source(
            root.clone(),
            parent,
        )?));
        self.do_xml_node(&root_builder, &root)?;
        Ok(root_builder)
    }

    fn load(&self) -> Result<NodeBuilderRef, Box<dyn Error>> {
        let main = self.exp_home.join("modules/main/flow.xml");
        if main.exists() {
            self.parse_xml(&main, None)
        } else {
            self.parse_xml(&self.exp_home.join("EntryModule/flow.xml"), None)
        }
    }
}

pub fn load_experiment(exp_home: &std::path::PathBuf) -> Result<NodeRef, Box<dyn Error>> {
    let exp_loader = ExperimentBuilder {
        exp_home: exp_home.clone(),
    };
    let r = exp_loader.load()?;
    let n = r
        .borrow()
        .build(exp_home.to_str().ok_or("pathbuf to str")?)?;
    Ok(n)
}

// pub fn simple_recursive_graph_print(nb: &Rc<RefCell<NodeBuilder>>, indent: &str) {
//     let nbb = nb.borrow();
//     let rnb = nbb.real_node.borrow();
//     println!(
//         "{}{} ({:?})",
//         indent,
//         nbb.real_node.borrow().name,
//         rnb.flow_type
//     );
//     let mut new_indent = indent.to_string();
//     new_indent.push_str("    ");
//     for c in &nbb.children {
//         simple_recursive_graph_print(&c, &new_indent);
//     }
// }

use crate::builder::load_experiment;
use crate::node::NodeRef;
use std::cell::RefCell;
use std::collections::HashMap;
use std::error::Error;
use std::rc::Rc;

#[derive(Debug)]
pub struct Experiment {
    pub exp_home: std::path::PathBuf,
    pub root_node: NodeRef,
}

impl Experiment {
    pub fn find_node_by_submits_path(&self, path: &str) -> Result<NodeRef, Box<dyn Error>> {
        let mut tokens: Vec<&str> = path[1..].split("/").collect();
        let mut node = self.root_node.clone();
        let root_name = node.borrow().name.clone();
        let path_root = tokens.get(0).ok_or("Path has zero tokens")?;
        if *path_root != root_name {
            return Err(format!(
                "First token in path: '{}' does not match experiment root node name: '{}'",
                path_root, root_name
            )
            .into());
        }
        tokens.remove(0);
        for t in tokens {
            let tmp = node.borrow().get_submits_by_name(t)?;
            node = tmp;
        }
        Ok(node)
    }
}

pub struct ExperimentStore {
    experiments: HashMap<std::path::PathBuf, Rc<RefCell<Experiment>>>,
}
// I tried to create a static global variable to hold said store but Rust is
// complaining that it is not thread safe and doesn't let me do it.  Rc and
// RefCell are non-thread safe, and need to be replaced with Arc (atomic
// reference counted) and Mutex which locks a mutex when you want to borrow.
// I tried that but then it complained that my types needed to implement Sync
// and/or Send and I stopped there because I didn't understand what I was
// supposed to do.  But I think that pretty much every Rc and RefCell would
// need to be changed to Arc and Mutex.
// static exp_storage : ExperimentStore = ExperimentStore{ experiments: std::collections::HashMap::new()};

impl ExperimentStore {
    pub fn new() -> Self {
        Self {
            experiments: std::collections::HashMap::new(),
        }
    }
    pub fn get_experiment(
        &mut self,
        exp_home: &std::path::PathBuf,
    ) -> Result<Rc<RefCell<Experiment>>, Box<dyn Error>> {
        if let Some(exp) = self.experiments.get(exp_home) {
            return Ok(exp.clone());
        }

        let n = load_experiment(&exp_home)?;
        let exp = Experiment {
            exp_home: exp_home.clone(),
            root_node: n,
        };
        let exp_rcrc = Rc::new(RefCell::new(exp));
        self.experiments.insert(exp_home.clone(), exp_rcrc.clone());
        Ok(exp_rcrc)
    }
}

pub fn is_experiment(p: &std::path::PathBuf) -> Result<bool, Box<dyn Error>> {
    let c = std::fs::read_dir(&p)?;
    let mut has_modules = false;
    let mut has_sequencing = false;
    for cf in c {
        if let Ok(cf) = cf {
            let name = cf
                .file_name()
                .to_str()
                .ok_or("PathBuf::to_str()")?
                .to_string();
            let path = cf.path();
            if name == "EntryModule" {
                return Ok(true);
            } else if name == "modules" && path.is_dir() {
                has_modules = true;
            } else if name == "sequencing" && path.is_dir() {
                has_sequencing = true;
            }
            if has_modules && has_sequencing {
                return Ok(true);
            }
        }
    }
    return Ok(false);
}

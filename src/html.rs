use crate::experiment::*;
use crate::node::{submits_path, FlowType, Node};
use std::rc::Rc;

use std::cell::RefCell;
use std::error::Error;

pub struct HTMLRenderer<'a> {
    pub exp_home: String,
    pub datestamp: String,
    pub x_size: i32,
    pub y_size: i32,
    pub x_offset: i32,
    pub y_offset: i32,
    pub output: &'a mut dyn std::io::Write,
}

impl<'a> HTMLRenderer<'a> {
    fn draw_task(&mut self, _n: &Rc<RefCell<Node>>) -> Result<(), Box<dyn Error>> {
        let width: i32 = (3 * self.x_size) / 2;
        let height: i32 = self.y_size;
        self.output.write(format!("<rect x=\"{}\", y=\"{}\", width=\"{}\" height=\"{}\" stroke=\"black\" stroke-width=\"3\" fill=\"blue\"/>\n", -width/2, -height/2, width, height).as_bytes())?;
        Ok(())
    }
    fn draw_module(&mut self, _n: &Rc<RefCell<Node>>) -> Result<(), Box<dyn Error>> {
        let width = 3 * self.x_size / 2;
        let height = 3 * self.y_size / 2;
        self.output.write(format!("<rect x=\"{}\", y=\"{}\", width=\"{}\" height=\"{}\" stroke=\"black\" stroke-width=\"3\" fill=\"green\"/>\n", -width/2, -height/2, width, height).as_bytes())?;
        Ok(())
    }

    fn draw_loop(&mut self, _n: &Rc<RefCell<Node>>) -> Result<(), Box<dyn Error>> {
        self.output.write(format!("<ellipse cx=\"{}\", cy=\"{}\", rx=\"{}\" ry=\"{}\", fill=\"cyan\", stroke=\"black\", stroke-width=\"5\"/>\n", 0, 0, self.x_size, self.y_size).as_bytes())?;
        Ok(())
    }

    fn draw_switch(&mut self, _n: &Rc<RefCell<Node>>) -> Result<(), Box<dyn Error>> {
        let big_w = 30;
        let w = 20;
        let big_h = 10;
        self.output.write(format!("<path d=\"M{} {} L{} {} L{} {} L{} {}Z\", fill=\"yellow\", stroke=\"black\", stroke-width=\"4\"\n/>", -big_w, big_h, -w, -big_h, big_w, -big_h, w, big_h).as_bytes())?;
        Ok(())
    }

    fn draw_switch_item(&mut self, _n: &Rc<RefCell<Node>>) -> Result<(), Box<dyn Error>> {
        self.output.write(format!("<circle cx=\"{}\", cy=\"{}\", r=\"{}\", fill=\"grey\", stroke=\"black\", stroke-width=\"5\"/>\n", 0, 0, self.x_size/4).as_bytes())?;
        Ok(())
    }

    fn draw_family(&mut self, _n: &Rc<RefCell<Node>>) -> Result<(), Box<dyn Error>> {
        let width = 8 * self.x_size / 7;
        let height = 10 * self.y_size / 7;
        self.output.write(format!("<rect x=\"{}\", y=\"{}\", width=\"{}\" height=\"{}\" stroke=\"black\" stroke-width=\"3\" fill=\"magenta\"/>\n", -width/2, -height/2, width, height).as_bytes())?;
        Ok(())
    }

    fn draw_foreach(&mut self, _n: &Rc<RefCell<Node>>) -> Result<(), Box<dyn Error>> {
        self.output.write(format!("<circle cx=\"{}\", cy=\"{}\", r=\"{}\", fill=\"red\", stroke=\"black\", stroke-width=\"5\"/>\n", 0, 0, self.x_size/4).as_bytes())?;
        Ok(())
    }

    fn draw_npass_task(&mut self, _n: &Rc<RefCell<Node>>) -> Result<(), Box<dyn Error>> {
        let width = (3 * self.x_size) / 2;
        let height = self.y_size;
        let border_radius = 5;
        self.output.write(format!("<rect x=\"{}\", y=\"{}\", width=\"{}\" height=\"{}\" stroke=\"black\" stroke-width=\"3\" fill=\"violet\" rx=\"{}px\" ry=\"{}px\"/>\n", -width/2, -height/2, width, height, border_radius, 2*border_radius).as_bytes())?;
        Ok(())
    }

    fn draw_node_dispatch(&mut self, n: &Rc<RefCell<Node>>) -> Result<(), Box<dyn Error>> {
        match n.borrow().flow_type {
            FlowType::Task => self.draw_task(n),
            FlowType::NPassTask => self.draw_npass_task(n),
            FlowType::Module => self.draw_module(n),
            FlowType::Family => self.draw_family(n),
            FlowType::Loop => self.draw_loop(n),
            FlowType::ForEach => self.draw_foreach(n),
            FlowType::Switch => self.draw_switch(n),
            FlowType::SwitchItem => self.draw_switch_item(n),
        }
    }

    // DrawSubtree draws the subtree t at the requested position relative to the
    // parent.  The function returns the vertical and horizontal space of the
    // drawn subtree in order to place the next subtree.
    fn draw_subtree(
        &mut self,
        n: &Rc<RefCell<Node>>,
        x: i32,
        dy: i32,
    ) -> Result<(i32, i32), Box<dyn Error>> {
        // Set the position of the Subtree relative to its parent.
        self.output
            .write(format!("<g transform=\"translate({},{})\">\n", x, dy).as_bytes())?;

        let x_total_size = x + self.x_offset;
        let mut y_total_size = 0;
        let end_x = self.x_offset;
        let node = n.borrow();
        // YUCK, there has to be a better way
        let mut strong_refs: Vec<Rc<RefCell<Node>>> = vec![];
        let to_visit = match node.flow_type {
            FlowType::Switch => &node.children,
            _ => {
                let weak_refs = &node.submits;
                for wr in weak_refs {
                    let sr = wr
                        .upgrade()
                        .expect("Weak refs in submits should never be null");
                    strong_refs.push(sr);
                }
                &strong_refs
            }
        };

        for sub in to_visit {
            // Line (0,0) -> (0,Y) -> (X,Y)
            self.output.write(format!("<path d=\"M{} {} L{} {} L{} {}\" stroke-width=\"3\" stroke=\"black\" fill=\"none\"/>\n", 0, 0, 0, y_total_size, end_x, y_total_size).as_bytes())?;

            let result = self.draw_subtree(sub, self.x_offset, y_total_size)?;

            y_total_size += result.1;
        }

        // Draw the single node
        let link = format!(
            "/nodeinfo?exp={}&node={}&datestamp={}",
            self.exp_home,
            submits_path(n),
            self.datestamp
        );
        self.output.write(format!("<a href=\"{}\" target=\"popup\" onclick=\"window.open('{}','popup','width=600,height=600'); return false;\">\n", link, link).as_bytes())?;
        self.draw_node_dispatch(n)?;

        let text_color = match n.borrow().flow_type {
            FlowType::NPassTask | FlowType::Loop => "black",
            _ => "white",
        };
        self.output.write(format!("<text x=\"{}\" y=\"{}\" fill=\"{}\" text-anchor=\"middle\" font-weight=\"bold\" font-size=\"9\" dominant-baseline=\"middle\">{}</text>\n", 0, 0, text_color, n.borrow().name).as_bytes())?;
        self.output.write(format!("</a>").as_bytes())?;

        self.output.write(format!("</g>").as_bytes())?;
        if y_total_size == 0 {
            y_total_size = self.y_offset;
        }

        Ok((x_total_size, y_total_size))
    }

    pub fn draw_html_page(&mut self, exp: &Rc<RefCell<Experiment>>) -> Result<(), Box<dyn Error>> {
        let exp_home = exp
            .borrow()
            .exp_home
            .to_str()
            .ok_or("PathBuf::to_str()")?
            .to_string();
        self.output.write(
            format!(
                "<!DOCTYPE html>
            <html lang=\"en\"> <head>
            <style>
            body {{
                // background-image: url('http://web.science.gc.ca/~phc001/webflow.jpg');
                background-color:grey;
              }}
              </style>
              </head>
            <body>
            <H2>{}</H2>
            <p>Node colors just represent the type of node to make it look pretty</p>
            <svg width=\"2000\" height=\"16000\" id=\"flowtree\" fill=\"black\">",
                &exp_home
            )
            .as_bytes(),
        )?;
        let n = &exp.borrow().root_node;
        self.draw_subtree(n, self.x_offset, self.y_offset)?;
        self.output
            .write(format!("</svg></body> </html>\n").as_bytes())?;
        Ok(())
    }
}

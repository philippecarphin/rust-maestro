use actix_web::{App, HttpServer};
use std::error::Error;

use nodegraph::experiment::{is_experiment, ExperimentStore};
use nodegraph::html::HTMLRenderer;
use nodegraph::node::{
    simple_recursive_node_graph_print, simple_recursive_submits_graph_print, NodeRef,
};

/*
 * TODOs:
 *      TODO: Implement NodeInfo
 *      TODO: Look into making a global persistent experiment store
 *            which will require understanding why it's not letting me
 *            creatint a static instance of ExperimentStore.
 *      TODO: Add real tests so I don't have to switch the names of my
 *            two main functions to decide which one is run.
 */

#[derive(serde::Deserialize, Debug)]
struct MaestroQuery {
    node: String,
    exp: String,
}

#[actix_web::get("/exp{path}*")]
async fn test_exp(
    path: actix_web::web::Path<String>,
) -> Result<actix_web::HttpResponse, Box<dyn Error>> {
    let mut resp = actix_web::HttpResponse::Ok();
    let mut exp_store = ExperimentStore::new();
    let exp_home = path.to_string();
    let result = exp_store.get_experiment(&std::path::PathBuf::from(&exp_home));
    if let Err(e) = result {
        let mut err_resp = actix_web::HttpResponse::InternalServerError();
        return Ok(err_resp.body(format!("Error loading experiment {exp_home}: {}", e)));
    }
    let exp = result.unwrap();
    let mut out = Vec::<u8>::new();
    {
        let mut h = HTMLRenderer {
            exp_home,
            datestamp: "now".to_string(),
            x_size: 51,
            y_size: 15,
            x_offset: 100,
            y_offset: 40,
            output: &mut out,
        };
        h.draw_html_page(&exp)?;
    }
    Ok(resp.body(out))
}

fn generate_list(output: &mut dyn std::io::Write, path: String) -> Result<(), Box<dyn Error>> {
    output.write(format!("<!DOCTYPE html> <html lang=\"en\"> <head> </head> <body>").as_bytes())?;
    let paths = std::fs::read_dir(path.to_string())?;
    let mut list_index = 0;
    for f in paths {
        list_index += 1;
        if let Ok(f) = f {
            let p = f.path(); //.to_str().ok_or("PathBuf::to_str()")?.to_string();
            if !p.is_dir() {
                continue;
            }
            let mut url = p.to_str().ok_or("PathBuf::to_str()")?.to_string();
            // TODO Perhaps I could have a route /show that either produces
            // the HTML for the experiment graph or the list produced by this
            // function instead of conditionally coding /exp or /list into
            // the HTML here.
            match is_experiment(&p) {
                Err(e) => {
                    println!("Error on is_experiment({:?}): {}", p, e);
                    continue;
                },
                Ok(is_exp) => {
                    if is_exp {
                        println!("path {:?} is experiment",p);
                        url.insert_str(0, "/exp");
                    } else {
                        println!("path {:?} is NOT experiment",p);
                        url.insert_str(0, "/list");
                    }
                }
            }
            // It's so annoying to deal with these PathBuf things.
            let path = p
                .to_str()
                .ok_or("How can a path not be convertible to a str!!!!")?;
            output.write(
                format!("<li><a href=\"{}\">{list_index}: {path}</a></li>", url).as_bytes(),
            )?;
        }
    }
    output.write("</body></html>".as_bytes())?;
    Ok(())
}

// #[actix_web::get("/exp{path}*")]
// async fn test_exp(path: actix_web::web::Path<String>) -> Result<actix_web::HttpResponse, Box<dyn Error>> {
#[actix_web::get("/list{path}*")]
async fn get_experiment_list(
    path: actix_web::web::Path<String>,
) -> Result<actix_web::HttpResponse, Box<dyn Error>> {
    println!("LIST: path={path}");

    let mut resp = actix_web::HttpResponse::Ok();
    let mut out = Vec::<u8>::new();
    generate_list(&mut out, path.to_string())?;

    Ok(resp.body(out))
}

fn nodeinfo_html(
    output: &mut dyn std::io::Write,
    node: &NodeRef,
) -> Result<(), Box<dyn Error>> {
    output.write("<html><body><table>\n".as_bytes())?;
    output.write(
        format!(
            "<tr><td>submits_path:</td><td>{}</td></tr>\n",
            node.borrow().submits_path()?
        )
        .as_bytes(),
    )?;
    output.write(
        format!(
            "<tr><td>container_path:</td><td>{}</td></tr>\n",
            node.borrow().container_path()?
        )
        .as_bytes(),
    )?;
    output.write(
        format!(
            "<tr><td>resource file:</td><td>{}</td></tr>\n",
            node.borrow().get_resource_filename()?
        )
        .as_bytes(),
    )?;
    output.write("</table></body></html>".as_bytes())?;
    Ok(())
}

// fn generate_list(output: Box<&mut dyn std::io::Write>, path: String) -> Result<(), Box<dyn Error>> {
#[actix_web::get("/nodeinfo")]
async fn nodeinfo_handler(
    query: actix_web::web::Query<MaestroQuery>,
) -> Result<actix_web::HttpResponse, Box<dyn Error>> {
    println!("NODEINFO request");
    println!("MaestroQuery: node:{}, exp:{}", query.node, query.exp);
    let mut exp_store = ExperimentStore::new();
    let exp = exp_store.get_experiment(&std::path::PathBuf::from(&query.exp))?;
    // persistent experiment store working
    let mut out = Vec::<u8>::new();
    nodeinfo_html(
        &mut out,
        &exp.borrow().find_node_by_submits_path(&query.node)?,
    )?;
    let mut resp = actix_web::HttpResponse::Ok();

    Ok(resp.body(out))
}

#[actix_web::main] // or #[tokio::main]
async fn main() -> std::io::Result<()> {
    let port = 8080;
    let address = "0.0.0.0";

    println!("Stargin server on {address}:{port}");
    HttpServer::new(|| {
        App::new()
            .service(test_exp)
            .service(get_experiment_list)
            .service(nodeinfo_handler)
    })
    .bind((address, port))?
    .run()
    .await
}

fn _demo() -> Result<(), Box<dyn std::error::Error>> {
    let mut exp_store = ExperimentStore::new();
    // let exp_home: std::path::PathBuf::from("mockfiles/dot-suites/philtest")
    let exp_home = std::path::PathBuf::from("mockfiles/dot-suites/g0");
    let exp = exp_store.get_experiment(&exp_home)?;
    let n = &exp.borrow().root_node;
    println!("================================ Graph Of Children (containers) ===================");
    simple_recursive_node_graph_print(n, "");
    println!("================================ Graph Of Submits ===============================");
    simple_recursive_submits_graph_print(&exp.borrow().root_node, "")?;
    Ok(())
}

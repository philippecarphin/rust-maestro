use std::cell::RefCell;
use std::error::Error;
use std::rc::{Rc, Weak};

use crate::resources;

pub type NodeRef = Rc<RefCell<Node>>;
pub type NodeWeakRef = Weak<RefCell<Node>>;

#[derive(Debug, Clone, Copy)]
pub enum FlowType {
    Module,
    Family,
    Task,
    NPassTask,
    Switch,
    SwitchItem,
    Loop,
    ForEach,
}

impl FlowType {
    pub fn from_string(s: &str) -> Option<Self> {
        Some(match s {
            "TASK" => Self::Task,
            "MODULE" => Self::Module,
            "FAMILY" => Self::Family,
            "NPASS_TASK" => Self::NPassTask,
            "SWITCH" => Self::Switch,
            "SWITCH_ITEM" => Self::SwitchItem,
            "LOOP" => Self::Loop,
            _ => return None,
        })
    }
}

pub enum RuntimeState {
    Submitted,
    Running,
    Aborted,
    Completed,
}

#[derive(Debug)]
pub struct Node {
    pub name: String,
    pub flow_type: FlowType,
    pub children: Vec<NodeRef>,
    pub parent: NodeWeakRef,
    pub depends_on: Vec<DepData>,
    pub submits: Vec<NodeWeakRef>,
    pub submitter: NodeWeakRef,
    pub resources: resources::NodeResources,
    // TODO: Maybe replace with weak ref to the experiment
    pub exp_home: String,
}

impl Node {
    pub fn new(name: String, flow_type: FlowType) -> Node {
        Node {
            name,
            flow_type,
            children: vec![],
            parent: Weak::new(),
            submits: vec![],
            depends_on: vec![],
            submitter: Weak::new(),
            resources: resources::NodeResources::new(),
            exp_home: "".to_string(),
        }
    }
    // Bunch of methods on the node type to calculate the various
    // things to put in the node info
    // - SubmitsPath : Build up a path from submitters
    // - ContainerPath: Build up a path from parents
    // - IntraModuleContainerPath: Build up a path from parents but stop
    //                             when we encounter a parent that is a Module
    // - PrecedingSiblings?
    // - NextSiblings?
    // - Dependencies: Return one line per dep composed of the non-Node fields
    // Of course add them with the other node methods, not in a
    // separate impl block
    pub fn get_child_by_name(&self, name: &str) -> Option<Rc<RefCell<Self>>> {
        for c in &self.children {
            if c.borrow().name == name {
                return Some(c.clone());
            }
        }
        return None;
    }
    pub fn get_submits_by_name(&self, name: &str) -> Result<Rc<RefCell<Self>>, Box<dyn Error>> {
        for c in &self.submits {
            let up_c = c
                .upgrade()
                .ok_or("Submits Weakref is None: Shouldn't happen")?;
            if up_c.borrow().name == name {
                return Ok(up_c.clone());
            }
        }
        return Err(format!("No submits with name {name}").into());
    }
    pub fn submits_path(&self) -> Result<String, Box<dyn Error>> {
        let mut submits_path = String::new();
        submits_path.insert_str(0, &self.name);
        submits_path.insert(0, '/');

        let mut sub = self.submitter.clone();
        loop {
            let maybe_submitter = sub.upgrade().clone();
            if maybe_submitter.is_none() {
                break;
            }
            let submitter = maybe_submitter.expect("Can't happen, checked for none above");
            submits_path.insert_str(0, &submitter.borrow().name);
            submits_path.insert(0, '/');
            sub = submitter.borrow().submitter.clone();
        }
        Ok(submits_path)
    }
    pub fn container_path(&self) -> Result<String, Box<dyn Error>> {
        let mut submits_path = String::new();
        submits_path.insert_str(0, &self.name);
        submits_path.insert(0, '/');

        let mut current_parent = self.parent.clone();
        loop {
            let maybe_parent = current_parent.upgrade().clone();
            if maybe_parent.is_none() {
                break;
            }
            let parent = maybe_parent.expect("Can't happen, checked for none above");
            submits_path.insert_str(0, &parent.borrow().name);
            submits_path.insert(0, '/');
            current_parent = parent.borrow().parent.clone();
        }
        Ok(submits_path)
    }

    pub fn get_resources(&mut self, datestamp: &str) -> Result<(), Box<dyn Error>> {
        let filename = self.get_resource_filename()?;
        self.resources = resources::NodeResources::from_xml_file(&filename, datestamp)?;
        Ok(())
    }

    pub fn get_resource_filename(&self) -> Result<String, Box<dyn Error>> {
        Ok(match self.flow_type {
            FlowType::Task | FlowType::NPassTask => {
                format!("{}/resources{}.xml", self.exp_home, self.container_path()?)
            }
            _ => {
                format!(
                    "{}/resources{}/container.xml",
                    self.exp_home,
                    self.container_path()?
                )
            }
        })
    }
}

#[derive(Debug)]
pub enum SeqDependsScope {
    IntraSuite,
    IntraUser,
    InterUser,
}

#[derive(Debug)]
pub enum SeqDependsType {
    Node,
    Date,
}

#[derive(Debug)]
pub struct DepData {
    pub dep_type: SeqDependsType,
    pub exp_scope: SeqDependsScope, // Internal, computed
    pub is_in_scope: bool,          // Computed
    pub node_name: String,
    pub node_path: Option<String>,
    pub exp: Option<String>,
    pub status: Option<String>, // RuntimeState?
    pub index: Option<String>,
    pub ext: Option<String>,
    pub local_index: Option<String>,
    pub local_ext: Option<String>,
    pub hour: Option<String>,
    pub time_delta: Option<String>,
    pub datestamp: Option<String>,
    pub valid_hour: Option<String>,
    pub valid_dow: Option<String>,
    pub protocol: Option<String>,
}

impl DepData {
    pub fn from_xml_source(xml: &elementtree::Element) -> Result<DepData, Box<dyn Error>> {
        Ok(DepData {
            dep_type: SeqDependsType::Node,         // TODO
            exp_scope: SeqDependsScope::IntraSuite, // TODO
            is_in_scope: true,                      // TODO
            node_name: xml
                .get_attr("dep_name")
                .ok_or("DEPENDS_ON node must have 'dep_name' attribute")?
                .to_string(),
            node_path: None, // TODO Compute from node name
            exp: xml.get_attr("exp").map(String::from),
            status: xml.get_attr("status").map(String::from),
            index: xml.get_attr("index").map(String::from),
            ext: xml.get_attr("ext").map(String::from),
            local_index: xml.get_attr("local_index").map(String::from),
            local_ext: xml.get_attr("local_ext").map(String::from),
            hour: xml.get_attr("hour").map(String::from),
            time_delta: xml.get_attr("time_delta").map(String::from),
            datestamp: xml.get_attr("datestamp").map(String::from),
            valid_hour: xml.get_attr("valid_hour").map(String::from),
            valid_dow: xml.get_attr("valid_dow").map(String::from),
            protocol: xml.get_attr("protocol").map(String::from),
        })
    }
}

pub fn submits_path(node: &Rc<RefCell<Node>>) -> String {
    let mut submits_path = String::new();
    let mut n = node.clone();
    loop {
        submits_path.insert_str(0, &n.borrow().name);
        submits_path.insert(0, '/');
        let tmp = n.borrow().submitter.upgrade().clone();
        if let Some(submitter) = tmp {
            n = submitter.clone()
        } else {
            break;
        }
    }
    submits_path
}

pub fn simple_recursive_node_graph_print(n: &Rc<RefCell<Node>>, indent: &str) {
    let nb = n.borrow();
    let parent_name = match nb.parent.upgrade() {
        Some(p) => p.borrow().name.clone(),
        None => "No parent".to_string(),
    };
    println!(
        "{}{} (type: {:?}, parent: {}, sc: {}) ",
        indent,
        nb.name,
        nb.flow_type,
        parent_name,
        Rc::strong_count(n)
    );
    let mut new_indent = indent.to_string();
    new_indent.push_str("    ");
    for s in &nb.submits {
        let real_sub = s
            .upgrade()
            .expect("Weak refs in Node submits should never be null");
        println!("{}       submits -> {}", indent, real_sub.borrow().name);
    }
    for c in &nb.children {
        simple_recursive_node_graph_print(&c, &new_indent);
    }
}

pub fn simple_recursive_submits_graph_print(
    n: &Rc<RefCell<Node>>,
    indent: &str,
) -> Result<(), Box<dyn Error>> {
    let nb = n.borrow();
    let parent_name = match nb.parent.upgrade() {
        Some(p) => p.borrow().name.clone(),
        None => "No parent".to_string(),
    };
    println!(
        "{}{} (type: {:?}, parent: {}, sc: {}) ",
        indent,
        nb.name,
        nb.flow_type,
        parent_name,
        Rc::strong_count(n)
    );
    let mut new_indent = indent.to_string();
    new_indent.push_str("    ");
    for s in &nb.submits {
        let rs = s
            .upgrade()
            .ok_or("Weak refs in Node submits should never be None")?;
        simple_recursive_submits_graph_print(&rs, &new_indent)?;
    }
    Ok(())
}

#![allow(dead_code)]
use std::error::Error;
use crate::node;

pub enum DayOfWeek {
    Sunday,
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
    Saturday,
}

pub struct ValidityData {
    dow: Option<DayOfWeek>,
    hour: Option<u8>,
    time_delta: Option<String>,
    valid_hour: Option<u8>,
    valid_dow: Option<DayOfWeek>,
    local_index: Option<String>,
}

impl ValidityData {
    fn from_xml_source(_e: &elementtree::Element) -> Result<Self, Box<dyn Error>> {
        Ok(Self {
            // TODO Actually look at the XML
            dow: Some(DayOfWeek::Monday),
            hour: Some(3),
            time_delta: Some("dt".to_string()),
            valid_hour: Some(8),
            valid_dow: Some(DayOfWeek::Tuesday),
            local_index: Some("+1+2".to_owned())
        })
    }
    fn is_valid(&self, _datestamp: &str) -> Result<bool, Box<dyn Error>> {
        Ok(true)
    }
}

#[derive(Debug)]
pub struct NodeResources {
    pub catchup: Option<u8>,
    pub mpi: Option<u32>,       // Maybe bool?
    pub wallclock: Option<u32>, // in seconds, we'll see
    pub is_last_arg: Option<bool>,
    pub immediate_mode: Option<bool>,
    pub queue: Option<String>,
    pub machine: Option<String>,
    pub memory: Option<String>,
    pub cpu: Option<u32>,
    pub cpu_multiplier: Option<String>,
    pub npex: Option<u32>,
    pub npey: Option<u32>,
    pub omp: Option<String>,
    pub soumet_args: Option<String>,
    pub work_queue: Option<String>,
    pub worker_path: Option<String>,
    pub depends_on: Vec<node::DepData>,
}

impl NodeResources {
    pub fn new() -> Self {
        NodeResources {
            catchup: None,
            mpi: None,
            wallclock: None,
            is_last_arg: None,
            immediate_mode: None,
            queue: None,
            machine: None,
            memory: None,
            cpu: None,
            cpu_multiplier: None,
            npex: None,
            npey: None,
            omp: None,
            soumet_args: None,
            work_queue: None,
            worker_path: None,
            depends_on: vec![],
        }
    }
    fn do_xml_node(&mut self, e: &elementtree::Element, datestamp: &str) -> Result<(), Box<dyn Error>> {
        for c in e.children() {
            let tag = c.tag().to_string();
            match tag.as_str() {
                "BATCH" => {
                    println!("Acquiring BATCH resources {:#?}", c);
                    self.acquire_batch_attributes(&c)?;
                },
                "DEPENDS_ON" => {
                    self.depends_on.push(node::DepData::from_xml_source(&c)?);
                }
                "VALIDITY" => {
                    let val = ValidityData::from_xml_source(c)?;
                    if val.is_valid(datestamp)? {
                        self.do_xml_node(c, datestamp)?;
                    }

                }
                _ => {
                    return Err(format!("Illegal node type '{}'", tag).into())
                }
            }
        }
        Ok(())
    }

    pub fn from_xml_file(filename: &str, _datestamp: &str) -> Result<Self, Box<dyn Error>> {
        let mut res = Self::new();
        let f = std::fs::File::open(filename)?;
        let root = elementtree::Element::from_reader(f)?;
        res.do_xml_node(&root, _datestamp)?;
        Ok(res)
    }
    fn xml_attr_to_u8(attr_name: &str, e: &elementtree::Element) -> Result<Option<u8>, Box<dyn Error>> 
    {
        let attr = e.get_attr(attr_name);
        if attr.is_none() {
            return Ok(None);
        }
        let /* mut */ attr_str = attr.unwrap().to_string();
        if attr_str.starts_with("${") {
            // TODO Evaluate ${}
            return Ok(Some(123));
        }

        let res = attr_str.parse::<u8>()?;
        Ok(Some(res))
    }
    fn xml_attr_to_u32(attr_name: &str, e: &elementtree::Element) -> Result<Option<u32>, Box<dyn Error>> 
    {
        let attr = e.get_attr(attr_name);
        if attr.is_none() {
            return Ok(None);
        }
        // TODO Evaluate ${}
        let /* mut */ attr_str = attr.unwrap().to_string();
        if attr_str.starts_with("${") {
            // TODO Evaluate ${}
            return Ok(Some(123));
        }

        let res = attr_str.parse::<u32>()?;
        Ok(Some(res))
    }
    fn xml_attr_to_bool(attr_name: &str, e: &elementtree::Element) -> Result<Option<bool>, Box<dyn Error>> 
    {
        let attr = e.get_attr(attr_name);
        if attr.is_none() {
            return Ok(None);
        }
        // TODO Evaluate ${}
        let /* mut */ attr_str = attr.unwrap().to_string();
        if attr_str.starts_with("${") {
            // TODO Evaluate ${}
            return Ok(Some(false));
        }

        match attr_str.as_str() {
            "TRUE"|"true" => Ok(Some(true)),
            "FALSE"|"false" => Ok(Some(false)),
            _ => Err("Cannot parse to bool".into())
        }
    }
    fn acquire_batch_attributes(&mut self, batch: &elementtree::Element) -> Result<(), Box<dyn Error>> {
        // TODO: For each attribute, the attribute should only be changed
        //       if we get back the Some variant.
        self.catchup = Self::xml_attr_to_u8("catchup", batch)?;
        self.mpi = Self::xml_attr_to_u32("mpi", batch)?;
        self.wallclock = Self::xml_attr_to_u32("wallclock", batch)?;
        self.is_last_arg = Self::xml_attr_to_bool("is_last_arg", batch)?;
        self.immediate_mode = Self::xml_attr_to_bool("immediate_mode", batch)?;
        self.queue = batch.get_attr("queue").map(String::from);
        self.machine = batch.get_attr("machine").map(String::from);
        self.memory = batch.get_attr("memory").map(String::from);
        self.cpu = Self::xml_attr_to_u32("cpu", batch)?;
        self.cpu_multiplier = batch.get_attr("cpu_multiplier").map(String::from);
        self.npex = Self::xml_attr_to_u32("npex", batch)?;
        self.npey = Self::xml_attr_to_u32("npey", batch)?;
        self.omp = batch.get_attr("omp").map(String::from);
        self.soumet_args = batch.get_attr("soumet_args").map(String::from);
        self.work_queue = batch.get_attr("work_queue").map(String::from);
        self.worker_path = batch.get_attr("worker_path").map(String::from);
        Ok(())
    }
}
#[test]
fn test_resource() -> Result<(), Box<dyn Error>> {
    let res = NodeResources::from_xml_file("mockfiles/dot-suites/g0/resources/main/cmdw_post_processing/container.xml", "202001020304")?;
    assert_eq!(res.catchup, Some(5));
    assert_eq!(res.cpu, Some(1));
    assert_eq!(res.machine, Some("${FRONTEND}".to_string()));
    Ok(())
}
